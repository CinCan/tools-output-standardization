from .__ResultGeneratorExtension__ import ResultGeneratorExtension
import os
from html import escape
from pathlib import Path


class peepdfResultGenerator(ResultGeneratorExtension):

    tool_name = "peepdf"

    def __init__(self, *args, **kwargs):
        super(peepdfResultGenerator, self).__init__(*args, **kwargs)

    def addBasicAnalysisData(self):
        self.addHeader(2, f"Peepdf analysis results")
        virustotal_info = f'VirusTotal was founded in 2004 as a free service that analyzes files and URLs for viruses, worms, trojans and other kinds of malicious content. Their goal is to make the internet a safer place through collaboration between members of the antivirus industry, researchers and end users of all kinds. {self.generateHrefNotation("Virustotal", "https://www.virustotal.com/")}'
        self.addFootNote(
            f"Listing files which are known as malicious by Virustotal.[footnote_id]",
            virustotal_info,
        )
        self.writeTextLine("NOTE: Only PDF files have been analyzed by this tool.")
        self.writeTextLine()
        self.writeTextLine(
            self.addItalicizedText(
                f"See more information about this tool in {self.generateHrefNotation('here', 'https://eternal-todo.com/tools/peepdf-pdf-analysis-tool')}."
            )
        )
        virustotal_list = []
        not_found_list = []
        result_file_object = {}

        if not self.result_data:
            self.writeTextLine()
            self.writeTextLine(
                "No suitable result files provided from peepdf to provide report. Maybe you did not have any PDF sample files?"
            )
            return

        self.addHeader(3, f"Files found from Virustotal")
        for result in self.result_data:
            result = result.get("data")
            detection = result.get("peepdf_analysis").get("basic").get("detection")
            if detection:
                result_file_object["SHA256"] = (
                    result.get("peepdf_analysis").get("basic").get("sha256")
                )
                result_file_object["Filename"] = (
                    result.get("peepdf_analysis").get("basic").get("filename")
                )
                result_file_object["AV Detection rate"] = (
                    result.get("peepdf_analysis")
                    .get("basic")
                    .get("detection")
                    .get("rate")
                )
                result_file_object["Report URL"] = self.generateHrefNotation(
                    self.addItalicizedText("URL"),
                    result.get("peepdf_analysis")
                    .get("basic")
                    .get("detection")
                    .get("report_link"),
                )
                # print(malc_file_object)
                virustotal_list.append(result_file_object.copy())
            else:
                result_file_object["SHA256"] = (
                    result.get("peepdf_analysis").get("basic").get("sha256")
                )
                result_file_object["Filename"] = (
                    result.get("peepdf_analysis").get("basic").get("filename")
                )
                not_found_list.append(result_file_object.copy())

        self.addTable(dictionary_list=virustotal_list)
        self.addHeader(3, f"PDF files NOT found from Virustotal")
        if not_found_list:
            self.addTable(dictionary_list=not_found_list)
        else:
            self.writeTextLine(
                self.addItalicizedText("All files were found from Virustotal.")
            )
        self.logger.debug("Peepdf analysis results generated.")

    def addAdvancedAnalysisData(self):

        if not self.result_data:
            return

        self.addHeader(3, f"Peepdf advanced analysis")
        self.writeTextLine(
            "More advanced analysis results for files one by one. It's possible that not all details are included. Check result JSON for all details. Titles for each file below includes detection rates of Virustotal antivirus filechecks"
        )
        for result in self.result_data:
            self.addHorizontalRule()
            basic_info = result.get("data").get("peepdf_analysis").get("basic")
            detection_rate = (
                basic_info.get("detection").get("rate")
                if basic_info.get("detection")
                else "Unknown"
            )
            self.insertDetailsAndSummary(
                f'<b>Detection {escape(detection_rate)}</b>: for {os.path.basename(escape(basic_info.get("filename")))} (Click to collapse.)',
                escape_html=False,
            )
            self.addHorizontalRule()
            # Wheather the given result location is absolute or relative path, generate relative
            p_filePath = Path(result.get("filepath")).resolve()
            relative_path = p_filePath.relative_to(self.filename.parent)
            self.writeTextLine(
                self.addItalicizedText(
                    f'Original result JSON can be found from {self.generateHrefNotation("here", relative_path)}'
                )
            )
            self.writeTextLine()
            self.writeTextLine(
                f'{self.addBoldedText("SHA256:")} {self.addInlineCodeBlock(basic_info.get("sha256"))}'
            )
            self.writeTextLine(
                f'{self.addBoldedText("SHA1:")} {basic_info.get("sha1")}'
            )
            self.writeTextLine(f'{self.addBoldedText("MD5:")} {basic_info.get("md5")}')
            self.writeTextLine(
                f'{self.addBoldedText("PDF version:")} {basic_info.get("pdf_version")}'
            )
            self.writeTextLine(
                f'{self.addBoldedText("Filesize (in bytes):")} {basic_info.get("size")}'
            )
            self.writeTextLine(
                f'{self.addBoldedText("Binary:")} {basic_info.get("binary")}'
            )
            self.writeTextLine(
                f'{self.addBoldedText("Number of objects:")} {basic_info.get("num_objects")}'
            )
            self.writeTextLine(
                f'{self.addBoldedText("Number of streams:")} {basic_info.get("num_streams")}'
            )
            self.writeTextLine(
                f'{self.addBoldedText("Encrypted:")} {basic_info.get("encrypted")}'
            )
            if basic_info.get("encryption_algorithms"):
                encr_algh = (
                    [
                        f'{self.addItalicizedText(str(alg.get("algorithm")))} - Bits: {self.addItalicizedText(str(alg.get("bits")))}'
                        for alg in basic_info.get("encryption_algorithms")
                    ]
                    if basic_info.get("encryption_algorithms")
                    else "Not identified."
                )
                self.writeTextLine(
                    f'{self.addBoldedText("Encryption algorithms:")} {", ".join(encr_algh)}'
                )
            self.writeTextLine(
                f'{self.addBoldedText("Linearized:")} {basic_info.get("linearized")}'
            )
            self.writeTextLine(
                f'{self.addBoldedText("Comments:")} {basic_info.get("comments")}'
            )

            self.addHeader(4, "File structure")
            advanced_info = result.get("data").get("peepdf_analysis").get("advanced")
            for version_info in advanced_info:
                version_info = version_info.get("version_info")
                version_detail = (
                    "Original file"
                    if version_info.get("version_number") == 0
                    else "Updated file from original"
                )
                # self.writeTextLine(
                #     f'{self.addBoldedText("File version: ")}{self.addHeader(4, version_detail)}'
                # )
                self.addHeader(5, version_detail)

                header_names = ["Object type", "Object numbers (Not amount!)"]
                row_elements = []
                all_objects = (
                    [
                        "All objects",
                        ", ".join(str(x) for x in version_info.get("objects")),
                    ]
                    if version_info.get("objects")
                    else None
                )
                if all_objects:
                    row_elements.append(all_objects)
                catalog = (
                    ["Catalog", version_info.get("catalog")]
                    if version_info.get("catalog")
                    else None
                )
                if catalog:
                    row_elements.append(catalog)
                info = (
                    ["Information", version_info.get("info")]
                    if version_info.get("info")
                    else None
                )
                if info:
                    row_elements.append(info)
                compressed_objects = (
                    [
                        "Compressed objects",
                        ", ".join(
                            str(x) for x in version_info.get("compressed_objects")
                        ),
                    ]
                    if version_info.get("compressed_objects")
                    else None
                )
                if compressed_objects:
                    row_elements.append(compressed_objects)

                streams = (
                    ["Streams", ", ".join(str(x) for x in version_info.get("streams"))]
                    if version_info.get("streams")
                    else None
                )
                if streams:
                    row_elements.append(streams)
                decoding_error_streams = (
                    [
                        "Decoding error streams",
                        ", ".join(
                            str(x) for x in version_info.get("decoding_error_streams")
                        ),
                    ]
                    if version_info.get("decoding_error_streams")
                    else None
                )
                if decoding_error_streams:
                    row_elements.append(decoding_error_streams)
                encoding_streams = (
                    [
                        "Encoded streams",
                        ", ".join(str(x) for x in version_info.get("encoded_streams")),
                    ]
                    if version_info.get("encoded_streams")
                    else None
                )
                if encoding_streams:
                    row_elements.append(encoding_streams)
                xref_streams = (
                    [
                        "Xref streams",
                        ", ".join(str(x) for x in version_info.get("xref_streams")),
                    ]
                    if version_info.get("xref_streams")
                    else None
                )
                if xref_streams:
                    row_elements.append(xref_streams)
                error_objects = (
                    [
                        "Error objects",
                        ", ".join(str(x) for x in version_info.get("error_objects")),
                    ]
                    if version_info.get("error_objects")
                    else None
                )
                if error_objects:
                    row_elements.append(error_objects)
                js_objects = (
                    [
                        "JavaScript objects",
                        ", ".join(str(x) for x in version_info.get("js_objects")),
                    ]
                    if version_info.get("js_objects")
                    else None
                )
                if js_objects:
                    row_elements.append(js_objects)

                self.addTable(
                    header_names,
                    row_elements if row_elements else ["No objects found.", "-"],
                )
                suspicious_elements = version_info.get("suspicious_elements")
                if suspicious_elements.get("elements"):
                    self.writeTextLine(self.addBoldedText("Suspicious elements"))
                    self.addTable(
                        header_names=["Element name", "Object number"],
                        dictionary_list=suspicious_elements.get("elements"),
                    )
                if suspicious_elements.get("actions"):
                    self.writeTextLine(f'{self.addBoldedText("Actions")}')
                    actions = suspicious_elements.get("actions")
                    dict_list = []
                    for key in actions:
                        tmp = {}
                        tmp["Action name"] = key
                        tmp["Object number"] = ", ".join(
                            str(x) for x in actions.get(key)
                        )
                        dict_list.append(tmp.copy())
                    self.addTable(dictionary_list=dict_list)

                if suspicious_elements.get("triggers"):
                    self.writeTextLine(self.addBoldedText("Triggers"))
                    triggers = suspicious_elements.get("triggers")
                    dict_list = []
                    for key in triggers:
                        tmp = {}
                        tmp["Trigger name"] = key
                        tmp["Object number"] = ", ".join(
                            str(x) for x in triggers.get(key)
                        )
                        dict_list.append(tmp.copy())
                    self.addTable(dictionary_list=dict_list)
                if suspicious_elements.get("urls"):
                    self.writeTextLine(
                        f'{self.addBoldedText("URLs:")} {suspicious_elements.get("urls")}'
                    )
                if suspicious_elements.get("js_vulns"):
                    self.writeTextLine(
                        f'{self.addBoldedText("JS vulneralibities:")} {", ".join(str(x) for x in suspicious_elements.get("js_vulns"))}'
                    )
            self.endDetailsAndSummary()
            self.logger.debug(f"{self.tool_name} generator finished successfully.")

