from .__ResultGeneratorExtension__ import ResultGeneratorExtension
from standardizer.markdown.config.conf import SHELLCODE_OUTPUT_DIR

# from html import escape
from collections import defaultdict


class jsunpacknsctestResultGenerator(ResultGeneratorExtension):
    """
    Extension to generate section for Markdown report showcasing possible readable shellcode.
    Readable shellcode is acquired with help of jsunpack-n and peepdf's sctest tool.
    """

    tool_name = "jsunpackn_sctest"

    def __init__(self, *args, **kwargs):
        super(jsunpacknsctestResultGenerator, self).__init__(*args, **kwargs)
        self.sorted_results_per_sample = defaultdict(list)
        self.sortResultFiles()
        # self.writeTextLine(f"Currently strings of all samples are in single file. See it {self.generateHrefNotation('in here.', self.result_data[0].get('filepath'))}")

    def sortResultFiles(self):
        for result in self.result_data:
            # Create dict key for each sample file sha256 hash, append results of that sample in list for that key's value
            self.sorted_results_per_sample[
                result.get("data").get("originalFileSHA256")
            ].append(result)

        # self.logger.debug(self.result_data)

    def addBasicAnalysisData(self):
        self.addHeader(2, "Jsunpack-n and peepdf's sctest analysis section")
        self.writeTextLine(
            "This section is attempting to show possible embedded"
            " shellcode/JavaScript from malware files. Tool 'jsunpack-n' is used for"
            " extracting the exploit from the malware, and peepdf's feature 'sctest' "
            "attempts to convert it (usually from shellcode) for higher level language to be more readable."
        )
        self.writeTextLine()
        self.writeTextLine(
            f'See more about jsunpack-n in {self.generateHrefNotation("here.", "https://github.com/urule99/jsunpack-n")}'
        )
        self.writeTextLine(
            f'See more about peepdf in {self.generateHrefNotation("here.", "https://github.com/jesparza/peepdf")}'
        )
        if not self.result_data:
            self.writeTextLine()
            self.writeTextLine(
                "No suitable result files found from jsunpack-n and sctest, unable to provide report. Maybe you did not have any PDF sample files or they all were classified malicious by Virustotal?"
            )

    def addAdvancedAnalysisData(self):

        if not self.result_data:
            return

        for result in self.sorted_results_per_sample.keys():
            self.logger.debug(f"Key is {result} for sctest result file.")
            # self.logger.debug(self.sorted_results_per_sample.get(result))

            self.addHorizontalRule()
            self.insertDetailsAndSummary(
                f"<b>Shellcode analysis </b> for `{result}` (Click to collapse.)",
                escape_html=False,
            )
            self.addHorizontalRule()
            self.writeTextLine(
                f'{self.addBoldedText("SHA256:")} {self.addInlineCodeBlock(result)}'
            )

            self.writeTextLine(
                f"Following code sections were found from document, they are not ordered in any way and might contain duplicates."
            )
            self.writeTextLine()
            for code_case in self.sorted_results_per_sample.get(result):
                relative_path = self.result_dir.relative_to(self.filename.parent)
                href = self.generateHrefNotation(
                    f'{relative_path/SHELLCODE_OUTPUT_DIR/result/code_case.get("data").get("shellCodeFileName")}',
                    f'{relative_path/SHELLCODE_OUTPUT_DIR/result/code_case.get("data").get("shellCodeFileName")}',
                )
                self.writeTextLine(f"Original shellcode can be found from path {href}")
                self.addCodeBlock(
                    code_case.get("data").get("shellCode"), escape_html=False
                )
            self.endDetailsAndSummary()
        if not self.sorted_results_per_sample.keys():
            self.writeTextLine()
            self.writeTextLine(
                f"Couldn't detect any shellcode/JavaScript from the sample PDF files."
            )
        self.logger.debug(f"{self.tool_name} generator finished successfully.")

