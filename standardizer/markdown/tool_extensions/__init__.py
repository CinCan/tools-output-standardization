from .ClamAVResultGenerator import ClamAVResultGenerator
from .peepdfResultGenerator import peepdfResultGenerator
from .PDFiDResultGenerator import PDFiDResultGenerator
from .stringsResultGenerator import stringsResultGenerator
from .olevbaResultGenerator import olevbaResultGenerator
from .jsunpackn_sctestResultGenerator import jsunpacknsctestResultGenerator
from .oledumpResultGenerator import oledumpResultGenerator

__all__ = [
    "ClamAVResultGenerator",
    "peepdfResultGenerator",
    "PDFiDResultGenerator",
    "stringsResultGenerator",
    "olevbaResultGenerator",
    "jsunpacknsctestResultGenerator",
    "oledumpResultGenerator",
]
# from os.path import dirname, basename, isfile, join
# import glob

# modules = glob.glob(join(dirname(__file__), "*.py"))
# __all__ = [
#     basename(f)[:-3] for f in modules if isfile(f) and not f.endswith("__init__.py")
# ]
