# PDFiD markdown generator from log files, NOT from JSON

from ..lib.markdowngenerator import MarkdownGenerator
from ..config import logger
from standardizer.json import pdfid_standardize

import hashlib
import os
from os import linesep


class PDFiDResultGenerator(MarkdownGenerator):
    def __init__(
        self,
        filenames,
        pdfid_filename="pdfid.log",
        requires_more_analysis_filename=None,
        clean_filename=None,
        malicious_filename=None,
        **kwargs,
    ):
        super(PDFiDResultGenerator, self).__init__(**kwargs)
        self.parsed_output = None
        self.filenames = filenames
        self.pdfid_filename = pdfid_filename
        self.malicious_filename = (
            malicious_filename if malicious_filename else "pdfid-malicious.log"
        )
        self.requires_more_analysis_filename = (
            requires_more_analysis_filename
            if requires_more_analysis_filename
            else "pdfid-requires-more-analysis.log"
        )
        self.clean_filename = clean_filename if clean_filename else "pdfid-clean.log"
        self.malicious_header_names = ["SHA256 Hash", "Filename"]
        self.malicious_data = None

        if self.pdfid_filename in self.filenames.keys():
            with open(self.filenames.get(self.pdfid_filename), "r") as file:
                self.parsed_output = pdfid_standardize.parse(file.read())
                logger.debug(
                    f"Log file for pdfid named as {self.pdfid_filename} found."
                )

    def checkTypeAndCreateTable(self, filename):
        if self.filenames.get(filename):
            logger.debug(f"{filename} found. Table will be created.")

            with open(self.filenames.get(filename), "r") as file:
                self.malicious_data = file.read()

            lines = self.malicious_data.split(linesep)
            temp = []
            for line in lines:
                if line.strip():
                    try:
                        temp.append([line.split(" ")[0], line.split(" ")[2]])
                    except IndexError as e:
                        logger.warning(
                            f"Line had less attributes than excepted. Skipping line: {e.args}"
                        )
                        continue
            self.addTable(self.malicious_header_names, temp, "center")
        else:
            self.writeTextLine("None of the input files.")
            logger.debug(f"{filename} not found. Table not created.")

    def addBasicAnalysisData(self):
        self.addHeader(2, f"PDFiD analysis results")
        self.writeTextLine(
            self.addItalicizedText(
                f"See more information about this tool in {self.generateHrefNotation('here', 'https://blog.didierstevens.com/programs/pdf-tools/')}."
            )
        )
        self.addHeader(4, "Likely malicious")
        self.checkTypeAndCreateTable(self.malicious_filename)
        self.addHeader(4, "Likely clean")
        self.checkTypeAndCreateTable(self.clean_filename)
        self.addHeader(4, "Requires more analysis")
        self.checkTypeAndCreateTable(self.requires_more_analysis_filename)

    def addAdvancedAnalysisData(self):
        self.addBasicAnalysisData()
        self.addHeader(3, f"PDFiD Advanced analysis details")
        self.writeTextLine(
            f'{self.addBoldedText(f"Amount of samples: ")} {self.parsed_output.get("parsed").get("total_samples")}'
        )
        self.writeTextLine(
            f'{self.addBoldedText(f"Total amount of malicious: ")} {self.parsed_output.get("parsed").get("amount_malicious")}'
        )
        self.writeTextLine(
            f'{self.addBoldedText(f"Total amount of clean: ")} {self.parsed_output.get("parsed").get("amount_clean")}'
        )
        # self.writeTextLine()
        self.insertDetailsAndSummary(
            "Advanced analysis of each file. (Click to collapse.)"
        )
        for index, single_result in enumerate(
            self.parsed_output.get("parsed").get("results")
        ):
            temp = []
            self.addHorizontalRule()
            self.addHeader(
                4, f"Sample file {os.path.basename(single_result.get('file_path'))}"
            )
            self.writeTextLine(
                f"{self.addBoldedText('PDFiD Version: ')}{single_result.get('pdfid_version')}"
            )
            del single_result["pdfid_version"]
            self.writeTextLine(
                f"{self.addBoldedText('Filename: ')}{os.path.basename(single_result.get('file_path'))}"
            )
            with open(
                self.filenames.get(os.path.basename(single_result.get("file_path"))),
                "rb",
                buffering=0,
            ) as f:
                # logger.debug(f)
                bytes = f.read()  # read entire file as bytes
                readable_hash = hashlib.sha256(bytes).hexdigest()
            self.writeTextLine(f'{self.addBoldedText(f"SHA256 Hash:")}{readable_hash}')
            del single_result["file_path"]
            self.writeTextLine(
                f"{self.addBoldedText('PDF header:')}{single_result.get('pdf_header')}"
            )
            del single_result["pdf_header"]
            self.writeTextLine()
            for element in single_result.keys():
                # if element == 'pdfid_version' or element ==
                temp.append([element, single_result.get(element)])

            headers = ["Element", "Count of occurrences"]
            self.addTable(headers, temp, "center")
        self.endDetailsAndSummary()
        # self.addUnorderedList(temp)

        # headers = ['items1', 'items2']
        # items = [['item1', 'item2'], ['item3', 'item4']]
        # self.addCenteredTable(headers, items)
