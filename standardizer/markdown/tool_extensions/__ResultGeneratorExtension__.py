import os
import json

from abc import ABCMeta, abstractmethod
from standardizer.markdown.lib import MarkdownGenerator
from pathlib import Path


class ResultGeneratorExtension(MarkdownGenerator, metaclass=ABCMeta):
    def __init__(self, configuration, *args, **kwargs):
        super(ResultGeneratorExtension, self).__init__(*args, **kwargs)
        self.tool_name = configuration.get("tool_name")
        self.result_dir_single_extension = Path(
            configuration.get("result_dir")
        ).resolve()
        self.result_dir = None  # Root directory for results, for all extensions
        self.result_data = []
        self.findResultFiles()

    def findResultFiles(self):
        self.logger.debug(f"Finding {self.tool_name} result files.")
        try:
            if os.path.exists(self.result_dir_single_extension):
                for root, directories, filenames in os.walk(
                    self.result_dir_single_extension
                ):
                    if filenames:
                        for file in filenames:
                            self.logger.debug(
                                f"Found {self.tool_name} result file {file}"
                            )
                            self.logger.debug(f"Root is {root}")
                            filepath = os.path.join(root, file)
                            if os.stat(filepath).st_size == 0:
                                self.logger.warning(
                                    f"Result file {file} is empty, skipping it."
                                )
                                continue
                            with open(filepath, "r") as result_data:
                                tmp_obj = {}
                                tmp_obj["filename"] = file
                                tmp_obj["filepath"] = filepath
                                if file.lower().endswith(".json"):
                                    tmp_obj["data"] = json.load(result_data)
                                    self.result_data.append(tmp_obj.copy())
                                else:
                                    tmp_obj["data"] = result_data.read()
                                    self.result_data.append(tmp_obj.copy())

                    else:
                        self.logger.warning(
                            f"No input files found for {self.tool_name!r} in regular ResultGeneratorExtension search. However, extension might have own search for it."
                        )
            else:
                self.logger.warning(
                    f"Result path not found for enabled tool {self.tool_name!r}"
                )
        except TypeError as e:
            self.logger.error(
                f"No result path found for tool {self.tool_name!r}. Path: {self.result_dir_single_extension!r} Skipping tool output. Error: {e}"
            )
            raise LookupError(f"No result path found for {self.tool_name!r}")

    def setRootResultDirPath(self, general_dir):
        self.result_dir = general_dir

    @abstractmethod
    def addBasicAnalysisData(self):
        pass

    @abstractmethod
    def addAdvancedAnalysisData(self):
        pass
