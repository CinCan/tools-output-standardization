from .__ResultGeneratorExtension__ import ResultGeneratorExtension
import os
from html import escape
from pathlib import Path


class PDFiDResultGenerator(ResultGeneratorExtension):

    tool_name = "pdfid"

    def __init__(self, *args, **kwargs):
        super(PDFiDResultGenerator, self).__init__(*args, **kwargs)
        self.malicious = []
        self.clean = []
        self.requires_more_analysis = []
        self.amount_valid_results = 0
        self.amount_invalid_results = 0
        self.triage_plugin_enabled = True
        self.classifySamples()

    def classifySamples(self):
        """
        Method for classifying PDFiD analysis results plugins.
        Currently works only with Triage plugin

        ## Triage Plugin Score ##
        Based on plugin information from its source:

        Score 1.0 means: 'Sample is likely malicious and requires further analysis'
        Score 0.75 means '/ObjStm detected, analyze sample with pdfid-objstm.bat'
        Score 0.5 means 'Sample is likely not malicious but requires further analysis'
        Score 0.6 means 'Sample is likely not malicious but could contain phishing or payload URL'
        Score 0.0 means 'Sample is likely not malicious, unless you suspect this is used in a targeted/sophisticated attack'

        In this case, anything between 1.0 and 0.0 is considered as requiring more analysis.

        Plugin version from 2017/10/29
        """
        if not self.result_data:
            self.logger.debug("No result files provided for pdfid")
            return
        if self.triage_plugin_enabled:
            for result in self.result_data:
                pdfid_basic_data = (
                    result.get("data").get("pdfid")
                    if result.get("data").get("pdfid")
                    else None
                )
                pdfid_plugin_data = (
                    result.get("data").get("plugin_data")
                    if result.get("data").get("plugin_data")
                    else None
                )
                if pdfid_basic_data and pdfid_plugin_data:
                    for plugin in pdfid_plugin_data:
                        if plugin.get("plugin_name") == "Triage plugin":
                            if plugin.get("score") == 1.0:
                                result["triage_result"] = "Likely malicious"
                                self.malicious.append(result)
                            elif plugin.get("score") == 0.0:
                                result["triage_result"] = "Likely clean"
                                self.clean.append(result)
                            else:
                                result["triage_result"] = "Requires more analysis"
                                self.requires_more_analysis.append(result)
                            self.amount_valid_results += 1
                else:
                    self.logger.warning(
                        f"Unsupported data format in file {result.get('filename')} in tool {self.tool_name}. Skipping file."
                    )
                    self.amount_invalid_results += 1
        else:
            self.logger.warning(
                f"Triage plugin not enabled in tool {self.tool_name}. Unable to classify samples. Showing only andvanced analysis."
            )

    def makeTableFromTriage(self, results):
        """
        Generates JSON array with Filename, SHA256 hash and plugin values from given PDFiD
        JSON result array.

        :param results: Array of PDFiD default JSON outputs. There is SHA256 as an additional attribute.
        It might not exist in all PDFiD versions.
        """
        if results:
            tmp_result_list = []
            for result in results:
                pdfid_basic_data = (
                    result.get("data").get("pdfid")
                    if result.get("data").get("pdfid")
                    else None
                )
                pdfid_plugin_data = (
                    result.get("data").get("plugin_data")
                    if result.get("data").get("plugin_data")
                    else None
                )
                tmp_result = {}
                tmp_result["SHA256 Hash"] = pdfid_basic_data.get("FileSHA256")
                tmp_result["Filename"] = os.path.basename(
                    pdfid_basic_data.get("filename")
                )
                if self.triage_plugin_enabled:
                    for plugin in pdfid_plugin_data:
                        if plugin.get("plugin_name") == "Triage plugin":
                            tmp_result["Triage plugin score"] = plugin.get("score")
                            tmp_result_list.append(tmp_result.copy())
            if self.triage_plugin_enabled:
                self.addTable(alignment="center", dictionary_list=tmp_result_list)
        else:
            self.writeTextLine("None of the input files.")
            self.logger.debug(
                f"List not found when trying to create classification tables in tool {self.tool_name}"
            )

    def addBasicAnalysisData(self):
        self.addHeader(2, f"PDFiD analysis results")
        if self.triage_plugin_enabled:
            self.writeTextLine(
                "By default, basic analysis is depending on PDFiD's triage plugin to fastly classify potential infected PDF samples from clean ones."
            )
            self.writeTextLine(
                "According to plugin source file, plugin score tells different kind of information as:"
            )
            score_list = []
            score_list.append(
                self.addFootNote(
                    "Score 1.0 means: 'Sample is likely malicious and requires further analysis'[footnote_id]",
                    "The maliciousness of file is classified based on the objects it contains. Suspicious objects are thought to be: '/JS', '/JavaScript', '/AA', '/OpenAction', '/AcroForm', '/JBIG2Decode', '/RichMedia', '/Launch', '/EmbeddedFile', '/XFA', '/Colors > 2^24' ",
                    write=False,
                )
            )
            score_list.append(
                "Score 0.75 means '/ObjStm detected, analyze sample with pdfid-objstm.bat'"
            )
            score_list.append(
                "Score 0.5 means 'Sample is likely not malicious but requires further analysis'"
            )
            score_list.append(
                "Score 0.6 means 'Sample is likely not malicious but could contain phishing or payload URL'"
            )
            score_list.append(
                "Score 0.0 means 'Sample is likely not malicious, unless you suspect this is used in a targeted/sophisticated attack'"
            )
            self.addUnorderedList(score_list)

            self.writeTextLine(
                "In this case, anything between 1.0 and 0.0 is classified as requiring more analysis. Malicious ones should be analysed further anyway."
            )
        else:
            self.writeTextLine("")

        self.writeTextLine()
        self.writeTextLine(
            self.addItalicizedText(
                f"See more general information about this tool in {self.generateHrefNotation('here', 'https://blog.didierstevens.com/programs/pdf-tools/')}."
            )
        )
        if not self.result_data:
            self.addHeader(
                4,
                "No result files provided by pdfid for report generation. Maybe you did not have any PDF sample files?",
            )
            return
        self.addHeader(4, "Likely malicious")
        self.makeTableFromTriage(self.malicious)
        self.addHeader(4, "Likely clean")
        self.makeTableFromTriage(self.clean)
        self.addHeader(4, "Requires more analysis")
        self.makeTableFromTriage(self.requires_more_analysis)

    def addAdvancedAnalysisData(self):
        # self.addBasicAnalysisData()
        if not self.result_data:
            return
        self.addHeader(3, f"PDFiD Advanced analysis details")
        self.writeTextLine(
            "More advanced details about analysed files one by one. The most precise data can be seen from original result JSON."
        )
        self.writeTextLine()
        self.writeTextLine(
            f'{self.addBoldedText(f"Amount of provided samples:")} {len(self.result_data)}'
        )
        self.writeTextLine(
            f'{self.addBoldedText(f"Amount of likely malicious:")} {len(self.malicious)}'
        )
        self.writeTextLine(
            f'{self.addBoldedText(f"Amount of likely clean:")} {len(self.clean)}'
        )
        self.writeTextLine(
            f'{self.addBoldedText(f"Amount of requiring more analysis:")} {len(self.requires_more_analysis)}'
        )
        self.writeTextLine()
        # self.insertDetailsAndSummary(
        #     "Advanced analysis of each file. (Click to collapse.)"
        # )
        if self.amount_invalid_results == 0:
            for result in self.result_data:
                pdfid_data = result.get("data").get("pdfid")
                self.addHorizontalRule()
                # This specific case requires HTML bold. No Markdown supported inside summary name.
                # Input manually escaped from possible HTML
                self.insertDetailsAndSummary(
                    f'<b>{escape(result.get("triage_result"))}</b>: sample file {os.path.basename(escape(pdfid_data.get("filename")))} (Click to collapse.)',
                    escape_html=False,
                )
                sha256 = pdfid_data.get("FileSHA256")
                self.addHorizontalRule()
                self.writeTextLine(
                    f'{self.addBoldedText("SHA256 Hash:")} {self.addInlineCodeBlock(sha256)}'
                )
                pdf_version = pdfid_data.get("header")
                self.writeTextLine(
                    f'{self.addBoldedText("PDF header version:")} {pdf_version}'
                )
                self.writeTextLine()
                # Wheather the given result location is absolute or relative path, generate relative path
                p_filePath = Path(result.get("filepath")).resolve()
                relative_path = p_filePath.relative_to(self.filename.parent)
                self.writeTextLine(
                    self.addItalicizedText(
                        f'Original result JSON can be found from {self.generateHrefNotation("here.", relative_path)}'
                    )
                )
                self.writeTextLine()
                self.writeTextLine(
                    self.addItalicizedText(
                        "Count of occurrences of specific objects in the sample:"
                    )
                )
                pdf_objects = pdfid_data.get("keywords").get("keyword")

                # Change order of attributes and rename
                pdf_objects_swapped_order = [
                    {
                        "Element": keyword.get("name"),
                        "Count of occurrences": keyword.get("count"),
                        "HexCodeCount": keyword.get("hexcodecount"),
                    }
                    for keyword in pdf_objects
                ]
                self.addTable(dictionary_list=pdf_objects_swapped_order)
                self.endDetailsAndSummary()
                self.logger.debug(f"{self.tool_name} generator finished successfully.")

