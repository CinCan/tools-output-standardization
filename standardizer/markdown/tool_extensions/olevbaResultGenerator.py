from .__ResultGeneratorExtension__ import ResultGeneratorExtension
import os


class olevbaResultGenerator(ResultGeneratorExtension):

    tool_name = "olevba"

    def __init__(self, *args, **kwargs):
        super(olevbaResultGenerator, self).__init__(*args, **kwargs)

    def addBasicAnalysisData(self):
        self.addHeader(2, "Olevba analysis data")
        self.writeTextLine(
            f"Olevba is a tool used for parsing OLE and OpenXML files such as MS office documents to detect VBA Macrocs and extract their source code. See more information about this tool in {self.generateHrefNotation('here.', 'https://github.com/decalage2/oletools/wiki/olevba')}"
        )
        self.writeTextLine(
            "This section attempts to provide results if some VBA macros have been found and to show their source codes."
        )
        if not self.result_data:
            self.writeTextLine()
            self.writeTextLine(
                "No suitable result files provided from olevba to provide report. Note that PDF files are not analysed by this tool."
            )
            return

        basic_result_table = []
        for result in self.result_data:
            # something_found = False
            # self.logger.debug(result)

            sha256 = os.path.basename(
                os.path.splitext(result.get("filename"))[0]
            ).split("_")[1]
            self.logger.debug(sha256)
            filename = os.path.basename(
                next(
                    (
                        obj.get("file")
                        for obj in result.get("data")
                        if obj.get("file") is not None
                    ),
                    None,
                )
            )
            # self.addHeader(3, f"File {sha256} results")
            not_found = f"In result file {result.get('filename')}: Olevba couldn't find any macros from the file."

            analysis = next(
                (
                    obj.get("analysis")
                    for obj in result.get("data")
                    if obj.get("analysis") is not None
                ),
                None,
            )

            result_obj = {
                "SHA256": sha256,
                "Filename": filename,
                "Macros found": "No"
                if not analysis
                else f"{self.addBoldedText('Yes')}, see advanced details.",
            }
            basic_result_table.append(result_obj)
            if not analysis:
                self.logger.debug(not_found)
        self.addTable(dictionary_list=basic_result_table)
        # self.logger.debug(self.result_data)

    def addAdvancedAnalysisData(self):

        if not self.result_data:
            return

        self.addHeader(3, "Olevba advanced analysis")
        for result in self.result_data:
            filename = os.path.basename(
                next(
                    (
                        obj.get("file")
                        for obj in result.get("data")
                        if obj.get("file") is not None
                    ),
                    None,
                )
            )
            not_found = f"No macros found"
            analysis = next(
                (
                    obj.get("analysis")
                    for obj in result.get("data")
                    if obj.get("analysis") is not None
                ),
                None,
            )
            self.addHorizontalRule()
            self.insertDetailsAndSummary(
                f"<b>{'No macros found' if not analysis else 'Macros found'}</b> for `{filename}` (Click to collapse.)",
                escape_html=False,
            )
            self.addHorizontalRule()
            self.writeTextLine(
                f'{self.addBoldedText("SHA256:")} {self.addInlineCodeBlock(os.path.basename(os.path.splitext(result.get("filename"))[0]).split("_")[1])}'
            )
            self.writeTextLine(f'{self.addBoldedText("Filename:")} {filename}')
            try:
                self.writeTextLine(
                    f'{self.addBoldedText("Document type:")} {result.get("data")[1].get("type") if result.get("data")[1].get("type") else "Unknown"}'
                )
            except IndexError:
                self.writeTextLine(f'{self.addBoldedText("Document type:")} Unknown')

            if analysis:
                self.writeTextLine(
                    f'{self.addBoldedText("Special keywords from document: ")}'
                )
                self.addTable(dictionary_list=analysis, capitalize_headers=True)
                self.writeTextLine()
                macros = next(
                    (
                        obj.get("macros")
                        for obj in result.get("data")
                        if obj.get("macros") is not None
                    ),
                    None,
                )
                if macros:
                    self.addHeader(4, f"Macros, total amount found: {len(macros)}")
                    for i, macro in enumerate(macros):
                        self.addHeader(5, f"Marco number {i + 1}.")
                        self.writeTextLine(
                            f'{self.addBoldedText("VBA filename:")} {os.path.basename(macro.get("vba_filename"))}'
                        )
                        self.writeTextLine(
                            f'{self.addBoldedText("Sub filename:")} {os.path.basename(macro.get("subfilename"))}'
                        )
                        if macro.get("ole_stream"):
                            self.writeTextLine(f'{self.addBoldedText("OLE stream:")}')
                            self.addCodeBlock(macro.get("ole_stream"))
                        if macro.get("code"):
                            self.writeTextLine()
                            self.writeTextLine(
                                f'{self.addBoldedText("Macro source code:")}'
                            )
                            self.addCodeBlock(macro.get("code"), escape_html=False)
            else:
                self.writeTextLine()
                self.writeTextLine(
                    f"For file {filename}: Olevba couldn't detect any macros."
                )
                self.logger.debug(not_found)
            self.endDetailsAndSummary()
            self.logger.debug(f"{self.tool_name} generator finished successfully.")
