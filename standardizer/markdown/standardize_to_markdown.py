import argparse

# import sys
import os
import logging

from standardizer.markdown.pipeline_reports import *
from standardizer.markdown.tool_extensions import *
from standardizer.markdown.pipeline_reports import __all__ as PIPELINES
from standardizer.markdown.tool_extensions import __all__ as TOOLS

# from standardizer.markdown.config.log import logger


def getEnabledExtensions(pipeline_name):
    # Currently following result generator tools are enabled for report output:{os.linesep}{f'{os.linesep}'.join(ENABLED_EXTENSIONS)}
    # tools_enabled_names = []
    # try:
    #     for extension_c in ENABLED_EXTENSIONS:
    #         tools_enabled_names.append(
    #             next(
    #                 EXTENSIONS_CONF.get(key).get("tool_name")
    #                 for key in EXTENSIONS_CONF.keys()
    #                 if EXTENSIONS_CONF.get(key).get("extension_name" == extension_c)
    #             )
    #         )
    # except KeyError as e:
    #     logger.error(
    #         f"Something went wrong when trying to acquire names of enabled extensions. Check configuration: {e}"
    #     )
    #     exit(1)
    pass


def initLogger(LOGGING_LEVEL):
    logger = logging.getLogger()

    logger.setLevel(LOGGING_LEVEL)

    # Formatter of log
    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    handler = logging.StreamHandler()
    handler.setLevel(LOGGING_LEVEL)
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    return logger


def main(argv=None):

    parser = argparse.ArgumentParser(
        prog="standardizer",
        description="Tool used to generate Markdown report from various malware analysis tools output result files. "
        "Main purpose is to generate report from pipelines used in the CinCan project. (https://cincan.io)",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    # Check from modules which pipeline reports are supported
    pipelines_info = []
    for pipeline in PIPELINES:
        pipeline_info = {
            "name": globals()[pipeline].pipeline_name,
            "description": globals()[pipeline].pipeline_description,
            "extension_name": pipeline,
        }
        pipelines_info.append(pipeline_info.copy())

    tools_info = []

    for tool in TOOLS:
        tool_info = {"name": globals()[tool].tool_name, "extension_name": tool}
        tools_info.append(tool_info.copy())

    parser.add_argument(
        "pipeline",
        type=str,
        help=f"Give the name of pipeline or tool whereof the report should be generated.{os.linesep}"
        f'Currently available pipelines are: {os.linesep}{f",{os.linesep}".join([p.get("name") for p in  pipelines_info])}'
        f' Currently available tools are: {os.linesep}{f",{os.linesep}".join([p.get("name") for p in  tools_info])}',
    )
    parser.add_argument(
        "results_path",
        type=str,
        help=f"Give the path of selected pipeline's results root directory. Each directory for each tool's results is found automatically by name. See 'conf.py' for more details.",
    )
    parser.add_argument(
        "-o",
        "--output",
        type=str,
        nargs="?",
        help=f"Output path/filename where into Markdown report will be generated.",
    )
    parser.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help=f"Verbose, show all debug messages when constructing report.",
    )
    # print(argv)
    if len(argv) == 0:
        print(
            f"{os.linesep}No arguments provided for Markdown report generator.{os.linesep}"
        )
        parser.print_help()
        exit(1)
    args = parser.parse_args(argv)

    selected_pipeline = next(
        (
            p.get("extension_name")
            for p in pipelines_info
            if p.get("name") == args.pipeline
        ),
        None,
    )

    selected_tool = None

    if not selected_pipeline:

        selected_tool = next(
            (
                t.get("extension_name")
                for t in tools_info
                if t.get("name") == args.pipeline
            ),
            None,
        )

    if not selected_pipeline and not selected_tool:
        print(f"{os.linesep}Unsupported pipeline or tool.{os.linesep}")
        parser.print_help()
        exit(1)

    # Check if verbose enabled for debug messages
    if args.verbose:
        LOGGING_LEVEL = logging.DEBUG

    else:
        # Default logging level is info
        LOGGING_LEVEL = logging.INFO

    logger = initLogger(LOGGING_LEVEL)

    # print("Supported pipeline selected")
    filename = args.output if args.output else None

    # Current implementation expects that pipeline report has generateBasicInformation and genExtensionData methods implemented and only needed for report generation
    if selected_pipeline:
        with globals()[selected_pipeline](
            filename=filename,
            result_dir=args.results_path,
            root_object=True,
            enable_write=False,
            logger=logger,
        ) as report:
            report.generateBasicInformation()
            report.genExtensionData()

    if selected_tool:
        # with globals()[SingleToolExtensionReport](
        #     filename=filename,
        #     extension=selected_tool,
        #     result_dir=args.results_path,
        #     root_object=True,
        #     enable_write=False,
        #     logger=logger,
        # ) as report:
        #     report.genExtensionData()

        print(
            f"{os.linesep}Not implemented yet. Only pipeline output fully supported currently.{os.linesep}"
        )
        parser.print_help()
        exit(1)


if __name__ == "__main__":
    main()
