from .__PipelineReport__ import PipelineReport
from ..config.conf import EXTENSIONS_CONF
from pathlib import Path

# WIP


class SingleToolExtensionReport(PipelineReport):

    pipeline_name = "single-extension-report-util"
    pipeline_description = f"This is report template to just generate data with single extension from it's tools output data files."

    def __init__(self, extension, *args, **kwargs):
        super(SingleToolExtensionReport, self).__init__(*args, **kwargs)

        self.extension = extension
        self.filename = (
            Path(f"{self.extension_name}_report.md").resolve()
            if self.filename is None
            else self.filename
        )

    def genExtensionData(self):

        extension_conf = next(
            (
                EXTENSIONS_CONF.get(key)
                for key in EXTENSIONS_CONF.keys()
                if EXTENSIONS_CONF.get(key).get("extension_name") == self.extension
            ),
            None,
        )
        if not extension_conf:
            self.logger.error(
                f"No configuration found for extension {self.extension!r}. Exiting."
            )
