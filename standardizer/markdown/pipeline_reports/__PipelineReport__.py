import os
from pathlib import Path
import uuid
import datetime
import json
from standardizer.markdown.lib import MarkdownGenerator
from standardizer.markdown.config.conf import (
    TOOL_VERSION_INFO_DIR,
)
from standardizer.markdown.config.syntax import HTML_SPACE


class PipelineReport(MarkdownGenerator):
    def __init__(self, result_dir=None, *args, **kwargs):
        super(PipelineReport, self).__init__(*args, **kwargs)
        # def __init__(self, file=None, description='Pipeline report'):

        self.result_dir = Path(result_dir).resolve() if result_dir else Path(os.getcwd()).resolve()
        if not os.path.exists(self.result_dir):
            self.logger.error(f"Given result path {self.result_dir} does not exist. Report not generated.")
            exit(1)
        # Generate GUID when initializing instance
        self.uuid = uuid.uuid4()
        if self.uuid.is_safe is not uuid.SafeUUID.safe:
            self.logger.warning(
                f"GUID {self.uuid.hex} was not generated in multiprocessing-safe way."
            )
        self.directorynames = []
        self.filenames = {}  # Dictionary with following key-value pairs - filename:path
        # Currently list of file from Virustotal folder
        self.sample_filenames = []
        # Information of samplefiles based of 'filetypes.log'
        self.sample_files = []
        self.tools_data = (
            {}
        )  # Dictionary with following key-value pairs - toolname:dictonary output data
        self.report_timestamp = None
        self.timestamp_in_datetime = None
        self.findFiles()
        self.parseSampleFileInformation()

        # Instances of all possible extensions
        self.extension_instances = []

    def findFiles(self, timestamp_filename="_timestamp_"):
        """
        Method for listing all directories and files recursively
        from defined result directory {self.result_dir}.

        Additionally reads possible timestamp of results from the file named as
        '_timestamp_'.

        :param timestamp_filename: Name of possible file including timestamp
        report
        """
        dir_obj = {"dirName": None, "dirPath": None}
        self.logger.debug(f"Finding files. Root path is '{self.result_dir}'")
        for root, directories, filenames in os.walk(self.result_dir):
            for directory in directories:
                dir_obj["dirName"] = directory
                dir_obj["dirPath"] = os.path.join(root, directory)
                self.directorynames.append(dir_obj.copy())
                self.logger.debug(f"Found directory: {os.path.join(root, directory)}")

            for filename in filenames:
                if os.path.isfile(os.path.join(root, filename)):
                    self.filenames[filename] = os.path.join(root, filename)
                    self.logger.debug(f"Found file: {os.path.join(root, filename)}")
                    # self.logger.debug(f"Parent is {os.path.basename(Path(os.path.join(root, filename)).parent)}")
                    if (
                        os.path.basename(Path(os.path.join(root, filename)).parent)
                        == "virustotal"
                    ):
                        self.logger.debug(
                            f"Sample file found from virustotal named as {filename}"
                        )
                        self.sample_filenames.append(filename)

        if timestamp_filename in self.filenames.keys():
            with open(self.filenames.get(timestamp_filename), "r") as file:
                self.report_timestamp = file.readline().rstrip(os.linesep)
                self.logger.debug(f"Timestamp of report is {self.report_timestamp}")

    def parseSampleFileInformation(self, filename="filetypes.json"):
        """
        Method for parsing JSON containing file type, sha256 and filename attributes.
        Each sample file should be listed in there.

        :param filename: Filename of the file containing sample file information

        NOTE: Following deprecated
        Method for parsing 'filetypes.log' result file,
        to acquire information from original sample files.
        It expects, that file has repeating lines of:

        Filename: CVE-2010-0188_PDF_05C415D57E92F43578D8DEA73932068B
        SHA256sum: f492374f04bdee33045b43cad57a301cf22e1ca03cdd2c3e90fd3dbdb8cb38a6
        Type: pdf

        :param filename: Filename of the file containing sample file information
        """
        # file_information = {"SHA256 Hash": None, "Filename": None, "Filetype": None}

        if filename in self.filenames.keys():

            with open(self.filenames.get(filename), "r") as file:
                self.sample_files = json.load(file)

                # NOTE: FOLLOWING DEPRECATED, USING JSON INSTEAD
                # Split lines by whitespace, to get actual data value
                # for line in file:
                #     if line.startswith("Filename:"):
                #         file_information["Filename"] = line.split()[1]
                #     elif line.startswith("SHA256sum:"):
                #         file_information["SHA256 Hash"] = line.split()[1]
                #     elif line.startswith("Type:"):
                #         file_information["Filetype"] = line.split()[1]
                #         self.sample_files.append(file_information.copy())
                #         # self.logger.debug(f"Appeding file {file_information[:]}")
                #         # New entry, new object
                #     else:
                #         self.logger.warning(f"Incorrect content in file {filename}")
                # raise ValueError(f"Incorrect content in file {filename}")
                # else:
                self.logger.debug(f"Sample files from {filename}:")
                self.logger.debug(self.sample_files)
        else:
            self.logger.error(
                f"File '{filename}' not found. Unable to produce sample file information for report.'"
            )

    def createAnalysingMethods(self):

        self.addHeader(2, f"Analysing methods")
        # self.writeTextLine()
        self.writeTextLine()
        self.writeTextLine()
        self.writeTextLine(
            "Following tools have been used for analysing in this report:"
        )
        tool_info = {}
        tool_info_list = []
        for root, directories, filenames in os.walk(TOOL_VERSION_INFO_DIR):
            for filename in filenames:
                self.logger.debug(f"Found version information with filename: {filename}")
                with open(os.path.join(root, filename), "r") as file:
                    tool_info["Tool name"] = filename
                    tool_info["Version Information"] = file.readline().rstrip(
                        os.linesep
                    )
                    tool_info_list.append(tool_info.copy())
        self.addTable(dictionary_list=tool_info_list)

    def generateBasicInformation(self):
        """
        Method for generating basic information for pipeline report.
        It should be same always, regardless of used tools.
        """
        attribute_name_format_space = 30

        if self.report_timestamp:
            self.timestamp_in_datetime = datetime.datetime.strptime(
                self.report_timestamp, "%a %b %d %X %Z %Y"
            )
            timestamp_in_epoch = self.timestamp_in_datetime.timestamp()
        else:
            self.logger.warning("No timestamp found. Closing program.")
            exit()
        self.logger.debug(f"Converted epoch time is: {timestamp_in_epoch}")
        self.addHeader(1, f"{self.pipeline_name} report {self.uuid.hex}")

        attribute_name1 = "Report identification value[footnote_id]:"
        padding = attribute_name_format_space - len(
            attribute_name1.replace("[footnote_id]", "")
        )
        self.logger.debug(f"Lenght of padding is {padding}")
        attribute_reference1 = (
            "Report identification value is UUID as a 32-character hexadecimal string."
        )

        self.addFootNote(
            f"{self.addBoldedAndItalicizedText(attribute_name1)} {HTML_SPACE*padding}{self.uuid.hex}",
            attribute_reference1,
        )

        attribute_name2 = "Pipeline finished at:"

        self.writeAttributeValuePairLine(
            (attribute_name2, self.report_timestamp), attribute_name_format_space
        )
        self.writeAttributeValuePairLine(
            (
                "This report was generated at: ",
                datetime.datetime.now(datetime.timezone.utc).strftime(
                    "%a %b %d %H:%M:%S %Z %Y"
                ),
            ),
            attribute_name_format_space,
        )
        attribute_name3 = "Amount of sample files:"
        attribute_value3 = len(self.sample_files)
        self.writeAttributeValuePairLine(
            (attribute_name3, attribute_value3), attribute_name_format_space
        )

        self.addHeader(3, f"Sample files")
        self.writeTextLine("Following samples were provided to be analysed:")
        self.addTable(
            ["SHA256 Hash", "Filename", "Filetype"], dictionary_list=self.sample_files
        )
        self.createAnalysingMethods()


