from standardizer.markdown.pipeline_reports.__PipelineReport__ import PipelineReport
from standardizer.markdown.tool_extensions import *
from standardizer.markdown.config.conf import EXTENSIONS_CONF
import os
from pathlib import Path


# List of enabled extension
# In this case, order here defines the structure of document
ENABLED_EXTENSIONS = [
    "ClamAVResultGenerator",
    "PDFiDResultGenerator",
    "peepdfResultGenerator",
    "jsunpacknsctestResultGenerator",
    "stringsResultGenerator",
    "oledumpResultGenerator",
    "olevbaResultGenerator",
]


class DocumentPipelineReport(PipelineReport):
    pipeline_name = "document-pipeline"
    pipeline_description = (
        f"This pipeline is used for analysing PDF files and other kind of document files such as MS Office documents to identify possible suspicious ones."
        f"Currently following result generator tools are enabled for report output:{os.linesep}{f'{os.linesep}'.join(ENABLED_EXTENSIONS)}"
    )

    def __init__(self, *args, **kwargs):
        super(DocumentPipelineReport, self).__init__(*args, **kwargs)
        # If default in use, change it pipeline report specific name, otherwise keep as name is given then as argument
        self.filename = (
            Path(f"{self.pipeline_name}_report_{self.uuid.hex}.md").resolve()
            if self.default_filename_on_use or self.filename is None
            else self.filename
        )

    def genExtensionData(self):
        """
        Method for generating Markdown output from enabled extensions. Order of enabled extensions defines the
        structure of the document. This report contains nothing extra but the data provided by these tools.

        Parent class PipelineReport generates basic data of report.
        """

        # Instance and run each extension
        for extension in ENABLED_EXTENSIONS:
            # Get correct configuration for extension by it's name
            extension_conf = next(
                (
                    EXTENSIONS_CONF.get(key)
                    for key in EXTENSIONS_CONF.keys()
                    if EXTENSIONS_CONF.get(key).get("extension_name") == extension
                ),
                None,
            )
            if not extension_conf:
                self.logger.error(
                    f"No configuration found for extension {extension!r}. Skipping section from document."
                )
                continue
            # Check directories from result path find corresponding result directory for each tool/extension
            directoryFound = False
            for directory in self.directorynames:
                # Results are generated only if corresponding result directory is found
                if os.path.basename(directory.get("dirPath")) == extension_conf.get(
                    "result_dir"
                ):
                    directoryFound = True
                    self.logger.debug(
                        f"Matched result directory {directory.get('dirPath')!r} for tool {extension_conf.get('tool_name')!r}"
                    )
                    extension_conf["result_dir"] = directory.get("dirPath")
                    # Each extension inherits shared information from root document, so they can append data to same file
                    # And table of content, footnotes etc. is generated correctly.
                    extension_inst = globals()[extension](
                        configuration=extension_conf,
                        filename=self.filename,
                        document=self.document,
                        tmp_dir=self.tmp_dir,
                        pending_footnote_references=self.pending_footnote_references,
                        footnote_index=self.footnote_index,
                        header_data_array=self.header_info,
                        header_index=self.header_index,
                        document_data_array=self.document_data_array,
                        enable_write=self.enable_write,
                    )
                    # Each extension has addBasic and addAdvanced data methods
                    extension_inst.setRootResultDirPath(self.result_dir)
                    extension_inst.addBasicAnalysisData()
                    extension_inst.addAdvancedAnalysisData()
                    # self.extension_instances.append(extension_inst)
                    break
            if not directoryFound:
                self.logger.warning(
                    f"No result directory found for {extension_conf.get('tool_name')}. Section for report not generated."
                )
        self.logger.info(
            f"Document-pipeline report {str(self.filename)!r} successfully created."
        )
