import json
import time


def read_output(path="../output/pdfid_output.txt"):
    with open(path, "r") as f:
        output = f.read()

    return output


def write_output(output, path="../output/pdfid_output_standardized.json"):
    with open(path, "w") as f:
        f.write(json.dumps(output, indent=4))


def parse(output):
    parsed_output = {"raw": None, "parsed": {}, "metadata": {"unix_timestamp": None}}

    lines = output.split("\n")
    single_result = {}
    results = []
    for line in lines:
        line = line.replace("\r", "")

        if "PDFiD " in line:
            if any(single_result):
                results.append(single_result)
                single_result = {}
            version = line.split(" ")[1]
            file_path = " ".join(line.split(" ")[2:])
            single_result["pdfid_version"] = version
            single_result["file_path"] = file_path

        if "PDF Header:" in line:
            single_result["pdf_header"] = line.split(" ")[-1]

        if line.split(" ")[-1].isdigit():
            single_result[" ".join(line.split(" ")[:-1]).rstrip().lstrip()] = int(
                line.split(" ")[-1]
            )

        if "Triage plugin score:" in line:
            single_result["triage_plugin_score"] = float(line.split(" ")[-1])

        if "Triage plugin instructions:" in line:
            single_result["triage_plugin_instructions"] = " ".join(line.split(" ")[3:])

        if "Results" in line:
            continue

        if "Total number of samples:" in line:
            parsed_output["parsed"]["total_samples"] = " ".join(line.split(" ")[4:])

        if "Malicious:" in line:
            parsed_output["parsed"]["amount_malicious"] = int(line.split(" ")[-1])

        if "Clean:" in line:
            parsed_output["parsed"]["amount_clean"] = int(line.split(" ")[-1])

    parsed_output["parsed"]["results"] = results
    parsed_output["raw"] = output
    parsed_output["metadata"]["unix_timestamp"] = time.time()

    return parsed_output


if __name__ == "__main__":
    output = read_output()
    standardized_output = parse(output)
    write_output(standardized_output)
