from bs4 import BeautifulSoup
import json
import time
import ntpath
import base64
import os


def read_output(path=""):
    url = path
    page = open(url)
    return page.read()


def write_output(output, path="../output/pdfxray_lite_output_standardized.json"):
    with open(path, "w") as f:
        f.write(json.dumps(output, indent=4))


def parse(output):
    parsed_output = {
        "raw": None,
        "parsed": {"general_info": {}, "objects": {}, "suspicious": "false"},
        "metadata": {"unix_timestamp": "None"},
    }

    object_field = {"id": []}

    soup = BeautifulSoup(output, features="html5lib")

    # Get all objects of "General Information"
    p = soup.find_all("p")
    for i in range(0, 5):
        info_field = str(p[i].get_text()).split(":")[0].lower().replace(" ", "_")
        parsed_output["parsed"]["general_info"][info_field] = (
            str(p[i].get_text()).split(":")[1].lstrip()
        )

    # Find all objects below "All Objects" title
    start = soup.find("font", text="All Objects").parent
    objects = start.find_next_siblings()
    i = 0
    for element in objects:
        if element.name == "h2":
            object_field = {"id": []}
            i = i + 1
            object_field["id"] = element.get_text()
            parsed_output["parsed"]["objects"][i] = object_field
        elif element.name == "h3":
            field_name = element.get_text().lower().replace(" ", "_").replace("?", "")
            # Change Suspicious to "true" if found in objects
            if "suspicious" in field_name:
                parsed_output["parsed"]["suspicious"] = True
        elif element.name == "p":
            field_content = element.get_text()
            object_field[field_name] = field_content

    # Add basic information fields
    parsed_output["raw"] = output
    parsed_output["metadata"]["unix_timestamp"] = time.time()

    return parsed_output


if __name__ == "__main__":
    output = read_output(path="../output/pdfxray_lite_output.html")
    standardized_output = parse(output)
    write_output(standardized_output)
