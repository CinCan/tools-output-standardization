import json
import time


def read_output(path="../output/binwalk_output.txt"):
    with open(path, "r") as f:
        output = f.read()

    return output


def write_output(output, path="../output/binwalk_output_standardized.json"):
    with open(path, "w") as f:
        f.write(json.dumps(output, indent=4))


def parse(output):
    parsed_output = {
        "raw": None,
        "parsed": {"table": {"decimal": [], "hexadecimal": [], "description": []}},
        "metadata": {"unix_timestamp": None},
    }

    lines = output.split("\n")

    table_ongoing = False
    for line in lines:
        line = line.replace("\r", "")

        if "DECIMAL" in line and "HEXADECIMAL" in line and "DESCRIPTION" in line:
            table_ongoing = True
            continue

        if line.strip().count("-") == len(line.strip()):
            continue

        if table_ongoing:
            ongoing_column = 0
            chars = line.split(" ")
            value = ""
            for i in range(len(chars)):
                char = chars[i]

                if ongoing_column == 2:
                    value = " ".join(chars[i:]).lstrip()
                    parsed_output["parsed"]["table"]["description"].append(value)
                    break

                if char == "" and chars[i - 1] != "":
                    ongoing_column += 1
                    value = ""
                    continue

                value += char

                if value != "":
                    if ongoing_column == 0:
                        parsed_output["parsed"]["table"]["decimal"].append(value)
                    elif ongoing_column == 1:
                        parsed_output["parsed"]["table"]["hexadecimal"].append(value)

        if "Scan Time:" in line:
            parsed_output["parsed"]["scan_time"] = line.split("Scan Time:")[-1].lstrip()
        elif "Target File:" in line:
            parsed_output["parsed"]["target_file"] = line.split("Target File:")[
                -1
            ].lstrip()
        elif "MD5 Checksum:" in line:
            parsed_output["parsed"]["md5_checksum"] = line.split("MD5 Checksum:")[
                -1
            ].lstrip()
        elif "Signatures:" in line:
            parsed_output["parsed"]["signatures"] = line.split("Signatures:")[
                -1
            ].lstrip()

    parsed_output["raw"] = output
    parsed_output["metadata"]["unix_timestamp"] = time.time()

    return parsed_output


if __name__ == "__main__":
    output = read_output()
    standardized_output = parse(output)
    write_output(standardized_output)
