import json
import time


def read_output(path="../output/pdf2john_output.txt"):
    with open(path, "r") as f:
        output = f.read()
    return output


def write_output(output, path="../output/pdf2john_output_standardized.json"):
    with open(path, "w") as f:
        f.write(json.dumps(output, indent=4))


def parse(output):
    parsed_output = {
        "raw": None,
        "parsed": {"file_path": [], "pdf_format": [], "pdf_hash": []},
        "metadata": {"unix_timestamp": None},
    }

    lines = output.split("\n")

    target_file = lines[0].split(":")[0]
    pdf_hash = lines[0].split(":")[1]
    pdf_format = "*".join(pdf_hash.split("*")[:6])

    parsed_output["parsed"]["file_path"] = target_file
    parsed_output["parsed"]["pdf_hash"] = pdf_hash
    parsed_output["parsed"]["pdf_format"] = pdf_format

    parsed_output["raw"] = output
    parsed_output["metadata"]["unix_timestamp"] = time.time()

    return parsed_output


if __name__ == "__main__":
    output = read_output()
    standardized_output = parse(output)
    write_output(standardized_output)
