import json
import time


def read_output(path="../output/strings_output.txt"):
    with open(path, "r") as f:
        output = f.read()

    return output


def write_output(output, path="../output/strings_output_standardized.json"):
    with open(path, "w") as f:
        f.write(json.dumps(output, indent=4))


def parse(output):
    parsed_output = {
        "raw": None,
        "parsed": {"strings": []},
        "metadata": {"unix_timestamp": None},
    }

    lines = output.split("\n")

    for line in lines:
        line = line.replace("\r", "")

        if len(line) == 0:
            continue

        parsed_output["parsed"]["strings"].append(line)

    parsed_output["raw"] = output
    parsed_output["metadata"]["unix_timestamp"] = time.time()

    return parsed_output


if __name__ == "__main__":
    output = read_output()
    standardized_output = parse(output)
    write_output(standardized_output)
