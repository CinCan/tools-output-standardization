import argparse
import json
from tqdm import tqdm

import standardizer.json.binwalk_standardize
import standardizer.json.pdf2john_standardize
import standardizer.json.pdfid_standardize
import standardizer.json.pdfxray_lite_standardize
import standardizer.json.strings_standardize


def print_summary(input_paths, output_paths, tools):
    for input_path, tool, output_path in zip(input_paths, tools, output_paths):
        print(
            "\nFile: {}\nParser: {}\nSave to: {}".format(input_path, tool, output_path)
        )


def main(argv=None):
    parser = argparse.ArgumentParser(description="Tool output standardizer into JSON format")
    parser.add_argument(
        "--quiet",
        "-q",
        help="No questions asked.",
        action="store_true"
    )
    parser.add_argument(
        "--input",
        "-i",
        dest="input_path",
        help="Path to input file(s). Example: file1,file2,file3 OR file1",
        required=True,
    )
    parser.add_argument(
        "--output",
        "-o",
        dest="output_path",
        help="Path to output file(s). Example: file1,file2,file3 OR file1",
        required=True,
    )
    parser.add_argument(
        "--tool",
        "-t",
        dest="tool",
        help="Which tool output to parse. Example: binwalk,pdf2john,pdfxray_lite OR binwalk",
        required=True,
    )

    if not argv:
        args = parser.parse_args()
    else:
        args = parser.parse_args(argv)

    if not args.input_path or not args.output_path or not args.tool:
        parser.print_help()
        exit()

    input_paths = args.input_path.split(",")
    output_paths = args.output_path.split(",")
    tools = args.tool.split(",")

    if len(tools) < len(input_paths):
        tools = tools * len(input_paths)

    print_summary(input_paths, output_paths, tools)

    if not args.quiet:
        proceed = input("\nContinue (y/n)?: ")

        if proceed != "y":
            exit()


    # Add new tool output parsers here
    methods = {
        "binwalk": standardizer.json.binwalk_standardize.parse,
        "pdf2john": standardizer.json.pdf2john_standardize.parse,
        "pdfid": standardizer.json.pdfid_standardize.parse,
        "pdfxray_lite": standardizer.json.pdfxray_lite_standardize.parse,
        "strings": standardizer.json.strings_standardize.parse,
    }

    for input_path, output_path, tool in tqdm(
        list(zip(input_paths, output_paths, tools)), desc="Standardizing outputs"
    ):
        with open(input_path, "rb") as f:
            input_data = str(f.read(), "latin-1")

        standardized_output = methods[tool](input_data)

        with open(output_path, "w") as f:
            f.write(json.dumps(standardized_output, indent=4))


if __name__ == "__main__":
    main()
