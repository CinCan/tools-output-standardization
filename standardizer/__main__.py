from .markdown import standardize_to_markdown
from .json import standardize_to_json
import os
import sys
import argparse


class Standardize:
    def __init__(self):

        usage = f"standardizer <command> [<args>]{os.linesep}"
        usage = "".join([usage, f"{os.linesep}Available commands are:{2* os.linesep}"])
        markdown_help = f'{"":5}{"markdown":15}To generate Markdown report of specific type from various tool result files.{os.linesep}'
        json_help = f'{"":5}{"json":15}To standardize specific tool output into the JSON format.{os.linesep}'
        usage = "".join([usage, markdown_help, json_help])

        parser = argparse.ArgumentParser(
            prog="standardizer",
            description="Tool used to standardize output of various tools in JSON output format or as Markdown report.",
            usage=usage,
        )
        parser.add_argument(
            "command", help="Subcommand to run. Each command has their own help."
        )
        # parse_args defaults to [1:] for args
        # rest of the args need to be excluded too, or validation will fail
        if len(sys.argv[1:]) == 0:
            print(f"{os.linesep}Missing required command.{os.linesep}")
            parser.print_help()
            exit(1)
        args = parser.parse_args(sys.argv[1:2])
        if not hasattr(self, args.command):
            print("Unrecognized command")
            parser.print_help()
            exit(1)
        # dispatch pattern to invoke method with identical name
        getattr(self, args.command)()

    def json(self):
        standardize_to_json.main(argv=sys.argv[2:])

    def markdown(self):
        standardize_to_markdown.main(argv=sys.argv[2:])


def main():
    Standardize()


if __name__ == "__main__":
    main()
