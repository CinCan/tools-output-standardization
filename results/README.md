# document-pipeline report 9a28a4d8ccc2442e8d6f30d932a13a13  
***Report identification value[^1]:***  9a28a4d8ccc2442e8d6f30d932a13a13  
***Pipeline finished at:*** Tue Sep 17 07:50:47 UTC 2019  
***This report was generated at:*** Tue Sep 17 07:50:48 UTC 2019  
***Amount of sample files:*** 13  
  
### Table of Contents  
  * [document-pipeline report 9a28a4d8ccc2442e8d6f30d932a13a13](#document-pipeline-report-9a28a4d8ccc2442e8d6f30d932a13a13)
      * [Sample files](#sample-files)
    * [Analysing methods](#analysing-methods)
    * [ClamAV analysis results](#clamav-analysis-results)
    * [PDFiD analysis results](#pdfid-analysis-results)
      * [PDFiD Advanced analysis details](#pdfid-advanced-analysis-details)
    * [Peepdf analysis results](#peepdf-analysis-results)
      * [Files found from Virustotal](#files-found-from-virustotal)
      * [PDF files NOT found from Virustotal](#pdf-files-not-found-from-virustotal)
      * [Peepdf advanced analysis](#peepdf-advanced-analysis)
    * [Footnotes](#footnotes)
  
### Sample files  
Following samples were provided to be analysed:  
  
| SHA256 Hash | Filename | Filetype |  
|:---:|:---:|:---:|  
| d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf | CVE-2010-0188_PDF_1C013154B528B332CD1552451F8AD1A3 | pdf |  
| 870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38 | CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 | pdf |  
| c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5 | CVE-2010-0188_PDF_6AC220194E8B53C70CFACD6D3ED1F455 | pdf |  
| 94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc | CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 | pdf |  
| ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f | CVE_2010-2883_PDF_83627FF5E21B8AD89224926CE9913110 | pdf |  
| bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc | CVE_2010-2883_PDF_AF6B71B2BA0E9DC903DC2CE677FFB5ED | pdf |  
| 38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e | CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 | pdf |  
| 2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415 | CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F | pdf |  
| dc79fe9c352907cecde56be7c335da60fa23044d547edf3f72128645e4184c98 | Notes.docx | vnd.openxmlformats-officedocument.wordprocessingml.document |  
| f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71 | Round_Table_Discussions.doc | msword |  
| e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 | tee.txt | x-empty |  
| a7fc11b5bcd8f0f4bd58d1b003202c2c92193adcf8f42fa30873cac92ba15cb9 | testi.bas | plain |  
| 67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac | sample.pdf | pdf |  
  
  
## Analysing methods  
  
  
Following tools have been used for analysing in this report:  
  
  
## ClamAV analysis results  
  
*Click on SHA256 sum*  to see original JSON output file. This contains some advanced details. Advanced details could contain additional metadata and possible components found from the sample file.  
  
See more information about ClamAV in [here.](https://www.clamav.net/)  
  
| SHA256 Hash | Filename | Malwares | Filesize (bytes) | ClamAV Filetype[^2] |  
|:---:|:---:|:---:|:---:|:---:|  
| [2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415](json-output/clamav/clamav_2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415.json) | CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F | **Pdf.Dropper.Agent-1828689**<br> | 45664 | CL_TYPE_PDF |  
| [38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e](json-output/clamav/clamav_38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e.json) | CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 | **Pdf.Dropper.Agent-1507032**<br> | 149087 | CL_TYPE_PDF |  
| [67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac](json-output/clamav/clamav_67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac.json) | sample.pdf | **Heuristics.PDF.ObfuscatedNameObject**<br> | 5672 | CL_TYPE_PDF |  
| [870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38](json-output/clamav/clamav_870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38.json) | CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 | **Pdf.Exploit.Dropped-90**<br> | 2685 | CL_TYPE_PDF |  
| [94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc](json-output/clamav/clamav_94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc.json) | CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 | **Pdf.Dropper.Agent-1506682**<br> | 1083750 | CL_TYPE_PDF |  
| [bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc](json-output/clamav/clamav_bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc.json) | CVE_2010-2883_PDF_AF6B71B2BA0E9DC903DC2CE677FFB5ED | **Heuristics.PDF.ObfuscatedNameObject**<br> **Pdf.Dropper.Agent-1506721**<br> | 45460 | CL_TYPE_PDF |  
| [c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5](json-output/clamav/clamav_c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5.json) | CVE-2010-0188_PDF_6AC220194E8B53C70CFACD6D3ED1F455 | **Pdf.Exploit.Dropped-90**<br> | 2696 | CL_TYPE_PDF |  
| [d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf](json-output/clamav/clamav_d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf.json) | CVE-2010-0188_PDF_1C013154B528B332CD1552451F8AD1A3 | **Pdf.Dropper.Agent-1506725**<br> | 2979 | CL_TYPE_PDF |  
| [dc79fe9c352907cecde56be7c335da60fa23044d547edf3f72128645e4184c98](json-output/clamav/clamav_dc79fe9c352907cecde56be7c335da60fa23044d547edf3f72128645e4184c98.json) | Notes.docx | *No known malwares found.*  | 13394 | CL_TYPE_OOXML_WORD |  
| [f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71](json-output/clamav/clamav_f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71.json) | Round_Table_Discussions.doc | **Doc.Dropper.Agent-1828517**<br> | 117086 | CL_TYPE_MSWORD |  
| [ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f](json-output/clamav/clamav_ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f.json) | CVE_2010-2883_PDF_83627FF5E21B8AD89224926CE9913110 | **Pdf.Dropper.Agent-1506750**<br> | 45802 | CL_TYPE_PDF |  
  
  
## PDFiD analysis results  
By default, basic analysis is depending on PDFiD&#x27;s triage plugin to fastly classify potential infected PDF samples from clean ones.  
According to plugin source file, plugin score tells different kind of information as:  
  * Score 1.0 means: &#x27;Sample is likely malicious and requires further analysis&#x27;[^3]
  * Score 0.75 means &#x27;/ObjStm detected, analyze sample with pdfid-objstm.bat&#x27;
  * Score 0.5 means &#x27;Sample is likely not malicious but requires further analysis&#x27;
  * Score 0.6 means &#x27;Sample is likely not malicious but could contain phishing or payload URL&#x27;
  * Score 0.0 means &#x27;Sample is likely not malicious, unless you suspect this is used in a targeted/sophisticated attack&#x27;
  
In this case, anything between 1.0 and 0.0 is classified as requiring more analysis. Malicious ones should be analysed further anyway.  
  
*See more general information about this tool in [here](https://blog.didierstevens.com/programs/pdf-tools/).*   
  
#### Likely malicious  
  
| SHA256 Hash | Filename | Triage plugin score |  
|:---:|:---:|:---:|  
| 2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415 | CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F | 1.0 |  
| 38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e | CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 | 1.0 |  
| 67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac | sample.pdf | 1.0 |  
| 870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38 | CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 | 1.0 |  
| 94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc | CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 | 1.0 |  
| bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc | CVE_2010-2883_PDF_AF6B71B2BA0E9DC903DC2CE677FFB5ED | 1.0 |  
| c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5 | CVE-2010-0188_PDF_6AC220194E8B53C70CFACD6D3ED1F455 | 1.0 |  
| d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf | CVE-2010-0188_PDF_1C013154B528B332CD1552451F8AD1A3 | 1.0 |  
| ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f | CVE_2010-2883_PDF_83627FF5E21B8AD89224926CE9913110 | 1.0 |  
  
  
#### Likely clean  
None of the input files.  
  
#### Requires more analysis  
None of the input files.  
  
### PDFiD Advanced analysis details  
More advanced details about analysed files one by one. The most precise data can be seen from original result JSON.  
  
**Amount of provided samples:** 9  
**Amount of likely malicious:** 9  
**Amount of likely clean:** 0  
**Amount of requiring more analysis:** 0  
  

---
  
<details>  
<summary><b>Likely malicious</b>: sample file CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F (Click to collapse.)</summary>  
  

---
  
**SHA256 Hash:** `2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415`  
**PDF header version:** %PDF-1.4  
  
*Original result JSON can be found from [here.](json-output/pdfid/pdfid_2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415.json)*   
  
*Count of occurrences of specific objects in the sample:*   
  
| Element | Count of occurrences | HexCodeCount |  
|:---:|:---:|:---:|  
| obj | 21 | 0 |  
| endobj | 21 | 0 |  
| stream | 6 | 0 |  
| endstream | 6 | 0 |  
| xref | 1 | 0 |  
| trailer | 1 | 0 |  
| startxref | 1 | 0 |  
| /Page | 1 | 0 |  
| /Encrypt | 0 | 0 |  
| /ObjStm | 0 | 0 |  
| /JS | 1 | 0 |  
| /JavaScript | 1 | 0 |  
| /AA | 0 | 0 |  
| /OpenAction | 1 | 0 |  
| /AcroForm | 1 | 0 |  
| /JBIG2Decode | 0 | 0 |  
| /RichMedia | 0 | 0 |  
| /Launch | 0 | 0 |  
| /EmbeddedFile | 0 | 0 |  
| /XFA | 1 | 0 |  
| /URI | 0 | 0 |  
| /Colors &gt; 2^24 | 0 | 0 |  
  
  
</details>  

---
  
<details>  
<summary><b>Likely malicious</b>: sample file CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 (Click to collapse.)</summary>  
  

---
  
**SHA256 Hash:** `38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e`  
**PDF header version:** %PDF-1.2  
  
*Original result JSON can be found from [here.](json-output/pdfid/pdfid_38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e.json)*   
  
*Count of occurrences of specific objects in the sample:*   
  
| Element | Count of occurrences | HexCodeCount |  
|:---:|:---:|:---:|  
| obj | 44 | 0 |  
| endobj | 44 | 0 |  
| stream | 13 | 0 |  
| endstream | 13 | 0 |  
| xref | 1 | 0 |  
| trailer | 1 | 0 |  
| startxref | 1 | 0 |  
| /Page | 2 | 0 |  
| /Encrypt | 0 | 0 |  
| /ObjStm | 0 | 0 |  
| /JS | 1 | 0 |  
| /JavaScript | 1 | 0 |  
| /AA | 0 | 0 |  
| /OpenAction | 1 | 0 |  
| /AcroForm | 1 | 0 |  
| /JBIG2Decode | 0 | 0 |  
| /RichMedia | 0 | 0 |  
| /Launch | 0 | 0 |  
| /EmbeddedFile | 0 | 0 |  
| /XFA | 1 | 0 |  
| /URI | 0 | 0 |  
| /Colors &gt; 2^24 | 0 | 0 |  
  
  
</details>  

---
  
<details>  
<summary><b>Likely malicious</b>: sample file sample.pdf (Click to collapse.)</summary>  
  

---
  
**SHA256 Hash:** `67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac`  
**PDF header version:** %PDF-1.5  
  
*Original result JSON can be found from [here.](json-output/pdfid/pdfid_67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac.json)*   
  
*Count of occurrences of specific objects in the sample:*   
  
| Element | Count of occurrences | HexCodeCount |  
|:---:|:---:|:---:|  
| obj | 6 | 0 |  
| endobj | 6 | 0 |  
| stream | 1 | 0 |  
| endstream | 1 | 0 |  
| xref | 1 | 0 |  
| trailer | 1 | 0 |  
| startxref | 1 | 0 |  
| /Page | 1 | 1 |  
| /Encrypt | 0 | 0 |  
| /ObjStm | 0 | 0 |  
| /JS | 1 | 1 |  
| /JavaScript | 1 | 1 |  
| /AA | 0 | 0 |  
| /OpenAction | 1 | 1 |  
| /AcroForm | 0 | 0 |  
| /JBIG2Decode | 0 | 0 |  
| /RichMedia | 0 | 0 |  
| /Launch | 0 | 0 |  
| /EmbeddedFile | 0 | 0 |  
| /XFA | 0 | 0 |  
| /URI | 0 | 0 |  
| /Colors &gt; 2^24 | 0 | 0 |  
  
  
</details>  

---
  
<details>  
<summary><b>Likely malicious</b>: sample file CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 (Click to collapse.)</summary>  
  

---
  
**SHA256 Hash:** `870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38`  
**PDF header version:** %PDF-1.5  
  
*Original result JSON can be found from [here.](json-output/pdfid/pdfid_870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38.json)*   
  
*Count of occurrences of specific objects in the sample:*   
  
| Element | Count of occurrences | HexCodeCount |  
|:---:|:---:|:---:|  
| obj | 8 | 0 |  
| endobj | 8 | 0 |  
| stream | 1 | 0 |  
| endstream | 1 | 0 |  
| xref | 1 | 0 |  
| trailer | 1 | 0 |  
| startxref | 1 | 0 |  
| /Page | 1 | 0 |  
| /Encrypt | 0 | 0 |  
| /ObjStm | 0 | 0 |  
| /JS | 0 | 0 |  
| /JavaScript | 0 | 0 |  
| /AA | 0 | 0 |  
| /OpenAction | 0 | 0 |  
| /AcroForm | 1 | 0 |  
| /JBIG2Decode | 0 | 0 |  
| /RichMedia | 0 | 0 |  
| /Launch | 0 | 0 |  
| /EmbeddedFile | 1 | 0 |  
| /XFA | 1 | 0 |  
| /URI | 0 | 0 |  
| /Colors &gt; 2^24 | 0 | 0 |  
  
  
</details>  

---
  
<details>  
<summary><b>Likely malicious</b>: sample file CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 (Click to collapse.)</summary>  
  

---
  
**SHA256 Hash:** `94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc`  
**PDF header version:** %PDF-1.4  
  
*Original result JSON can be found from [here.](json-output/pdfid/pdfid_94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc.json)*   
  
*Count of occurrences of specific objects in the sample:*   
  
| Element | Count of occurrences | HexCodeCount |  
|:---:|:---:|:---:|  
| obj | 60 | 0 |  
| endobj | 60 | 0 |  
| stream | 20 | 0 |  
| endstream | 20 | 0 |  
| xref | 1 | 0 |  
| trailer | 1 | 0 |  
| startxref | 1 | 0 |  
| /Page | 6 | 0 |  
| /Encrypt | 1 | 0 |  
| /ObjStm | 0 | 0 |  
| /JS | 1 | 0 |  
| /JavaScript | 2 | 0 |  
| /AA | 1 | 0 |  
| /OpenAction | 0 | 0 |  
| /AcroForm | 1 | 0 |  
| /JBIG2Decode | 0 | 0 |  
| /RichMedia | 0 | 0 |  
| /Launch | 0 | 0 |  
| /EmbeddedFile | 0 | 0 |  
| /XFA | 1 | 0 |  
| /URI | 0 | 0 |  
| /Colors &gt; 2^24 | 0 | 0 |  
  
  
</details>  

---
  
<details>  
<summary><b>Likely malicious</b>: sample file CVE_2010-2883_PDF_AF6B71B2BA0E9DC903DC2CE677FFB5ED (Click to collapse.)</summary>  
  

---
  
**SHA256 Hash:** `bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc`  
**PDF header version:** %PDF-1.5  
  
*Original result JSON can be found from [here.](json-output/pdfid/pdfid_bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc.json)*   
  
*Count of occurrences of specific objects in the sample:*   
  
| Element | Count of occurrences | HexCodeCount |  
|:---:|:---:|:---:|  
| obj | 14 | 0 |  
| endobj | 14 | 0 |  
| stream | 4 | 0 |  
| endstream | 4 | 0 |  
| xref | 1 | 0 |  
| trailer | 1 | 0 |  
| startxref | 1 | 0 |  
| /Page | 1 | 1 |  
| /Encrypt | 0 | 0 |  
| /ObjStm | 0 | 0 |  
| /JS | 1 | 0 |  
| /JavaScript | 1 | 1 |  
| /AA | 0 | 0 |  
| /OpenAction | 1 | 1 |  
| /AcroForm | 1 | 1 |  
| /JBIG2Decode | 0 | 0 |  
| /RichMedia | 0 | 0 |  
| /Launch | 0 | 0 |  
| /EmbeddedFile | 0 | 0 |  
| /XFA | 1 | 1 |  
| /URI | 0 | 0 |  
| /Colors &gt; 2^24 | 0 | 0 |  
  
  
</details>  

---
  
<details>  
<summary><b>Likely malicious</b>: sample file CVE-2010-0188_PDF_6AC220194E8B53C70CFACD6D3ED1F455 (Click to collapse.)</summary>  
  

---
  
**SHA256 Hash:** `c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5`  
**PDF header version:** %PDF-1.5  
  
*Original result JSON can be found from [here.](json-output/pdfid/pdfid_c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5.json)*   
  
*Count of occurrences of specific objects in the sample:*   
  
| Element | Count of occurrences | HexCodeCount |  
|:---:|:---:|:---:|  
| obj | 8 | 0 |  
| endobj | 8 | 0 |  
| stream | 1 | 0 |  
| endstream | 1 | 0 |  
| xref | 1 | 0 |  
| trailer | 1 | 0 |  
| startxref | 1 | 0 |  
| /Page | 1 | 0 |  
| /Encrypt | 0 | 0 |  
| /ObjStm | 0 | 0 |  
| /JS | 0 | 0 |  
| /JavaScript | 0 | 0 |  
| /AA | 0 | 0 |  
| /OpenAction | 0 | 0 |  
| /AcroForm | 1 | 0 |  
| /JBIG2Decode | 0 | 0 |  
| /RichMedia | 0 | 0 |  
| /Launch | 0 | 0 |  
| /EmbeddedFile | 1 | 0 |  
| /XFA | 1 | 0 |  
| /URI | 0 | 0 |  
| /Colors &gt; 2^24 | 0 | 0 |  
  
  
</details>  

---
  
<details>  
<summary><b>Likely malicious</b>: sample file CVE-2010-0188_PDF_1C013154B528B332CD1552451F8AD1A3 (Click to collapse.)</summary>  
  

---
  
**SHA256 Hash:** `d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf`  
**PDF header version:** %PDF-1.6  
  
*Original result JSON can be found from [here.](json-output/pdfid/pdfid_d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf.json)*   
  
*Count of occurrences of specific objects in the sample:*   
  
| Element | Count of occurrences | HexCodeCount |  
|:---:|:---:|:---:|  
| obj | 8 | 0 |  
| endobj | 8 | 0 |  
| stream | 1 | 0 |  
| endstream | 1 | 0 |  
| xref | 1 | 0 |  
| trailer | 1 | 0 |  
| startxref | 1 | 0 |  
| /Page | 1 | 0 |  
| /Encrypt | 0 | 0 |  
| /ObjStm | 0 | 0 |  
| /JS | 0 | 0 |  
| /JavaScript | 0 | 0 |  
| /AA | 0 | 0 |  
| /OpenAction | 0 | 0 |  
| /AcroForm | 1 | 0 |  
| /JBIG2Decode | 0 | 0 |  
| /RichMedia | 0 | 0 |  
| /Launch | 0 | 0 |  
| /EmbeddedFile | 1 | 0 |  
| /XFA | 1 | 0 |  
| /URI | 0 | 0 |  
| /Colors &gt; 2^24 | 0 | 0 |  
  
  
</details>  

---
  
<details>  
<summary><b>Likely malicious</b>: sample file CVE_2010-2883_PDF_83627FF5E21B8AD89224926CE9913110 (Click to collapse.)</summary>  
  

---
  
**SHA256 Hash:** `ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f`  
**PDF header version:** %PDF-1.5  
  
*Original result JSON can be found from [here.](json-output/pdfid/pdfid_ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f.json)*   
  
*Count of occurrences of specific objects in the sample:*   
  
| Element | Count of occurrences | HexCodeCount |  
|:---:|:---:|:---:|  
| obj | 14 | 0 |  
| endobj | 14 | 0 |  
| stream | 4 | 0 |  
| endstream | 4 | 0 |  
| xref | 1 | 0 |  
| trailer | 1 | 0 |  
| startxref | 1 | 0 |  
| /Page | 1 | 0 |  
| /Encrypt | 0 | 0 |  
| /ObjStm | 0 | 0 |  
| /JS | 1 | 0 |  
| /JavaScript | 1 | 0 |  
| /AA | 0 | 0 |  
| /OpenAction | 1 | 0 |  
| /AcroForm | 1 | 0 |  
| /JBIG2Decode | 0 | 0 |  
| /RichMedia | 0 | 0 |  
| /Launch | 0 | 0 |  
| /EmbeddedFile | 0 | 0 |  
| /XFA | 1 | 0 |  
| /URI | 0 | 0 |  
| /Colors &gt; 2^24 | 0 | 0 |  
  
  
</details>  
  
## Peepdf analysis results  
Listing files which are known as malicious by Virustotal.[^4]  
NOTE: Only PDF files have been analyzed by this tool.  
  
*See more information about this tool in [here](https://eternal-todo.com/tools/peepdf-pdf-analysis-tool).*   
  
### Files found from Virustotal  
  
| SHA256 | Filename | AV Detection rate | Report URL |  
|:---:|:---:|:---:|:---:|  
| 2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415 | CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F | 37/59 | [*URL* ](https://www.virustotal.com/file/2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415/analysis/1542707772/) |  
| 38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e | CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 | 36/59 | [*URL* ](https://www.virustotal.com/file/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/analysis/1542707855/) |  
| 67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac | sample.pdf | 34/59 | [*URL* ](https://www.virustotal.com/file/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/analysis/1556800394/) |  
| 870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38 | CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 | 40/59 | [*URL* ](https://www.virustotal.com/file/870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38/analysis/1542708305/) |  
| 94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc | CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 | 33/58 | [*URL* ](https://www.virustotal.com/file/94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc/analysis/1542708409/) |  
| bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc | CVE_2010-2883_PDF_AF6B71B2BA0E9DC903DC2CE677FFB5ED | 44/59 | [*URL* ](https://www.virustotal.com/file/bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc/analysis/1542708588/) |  
| c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5 | CVE-2010-0188_PDF_6AC220194E8B53C70CFACD6D3ED1F455 | 40/59 | [*URL* ](https://www.virustotal.com/file/c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5/analysis/1542708640/) |  
| d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf | CVE-2010-0188_PDF_1C013154B528B332CD1552451F8AD1A3 | 40/58 | [*URL* ](https://www.virustotal.com/file/d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf/analysis/1542708763/) |  
| ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f | CVE_2010-2883_PDF_83627FF5E21B8AD89224926CE9913110 | 46/59 | [*URL* ](https://www.virustotal.com/file/ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f/analysis/1568541722/) |  
  
  
### PDF files NOT found from Virustotal  
*All files were found from Virustotal.*   
  
### Peepdf advanced analysis  
More advanced analysis results for files one by one. It&#x27;s possible that not all details are included. Check result JSON for all details. Titles for each file below includes detection rates of Virustotal antivirus filechecks  

---
  
<details>  
<summary><b>Detection 37/59</b>: for CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](json-output/peepdf/peepdf_2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415.json)*   
  
**SHA256:** `2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415`  
**SHA1:** 422c76ba5e2210acb80690bcd0cc2146c706119e  
**MD5:** bae689ecac665833b71670752ded0c4f  
**PDF version:** 1.4  
**Filesize (in bytes):** 45664  
**Binary:** True  
**Number of objects:** 21  
**Number of streams:** 6  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 23, 24 |  
| Catalog | 1 |  
| Information | 4 |  
| Streams | 24, 5, 7, 10, 17, 17 |  
| Encoded streams | 5, 7, 17, 17 |  
| JavaScript objects | 5, 7 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| CoolType.SING.uniqueName | 17<br> | CVE-2010-2883<br> | CoolType.SING.uniqueName |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 3 |  
| /JavaScript | 3 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 1 |  
| /OpenAction | 1 |  
| /XFA | 23 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 36/59</b>: for CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](json-output/peepdf/peepdf_38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e.json)*   
  
**SHA256:** `38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e`  
**SHA1:** 17acd4de827e726c727c0c8cc9c4e872a328ee6c  
**MD5:** bab80236fbaa1e241cc0f856c4351a84  
**PDF version:** 1.2  
**Filesize (in bytes):** 149087  
**Binary:** True  
**Number of objects:** 44  
**Number of streams:** 13  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44 |  
| Catalog | 1 |  
| Information | 2 |  
| Streams | 5, 21, 22, 23, 24, 25, 28, 30, 32, 34, 40, 43, 44 |  
| Encoded streams | 5, 21, 22, 23, 24, 25, 28, 30, 32, 34, 40, 43, 44 |  
| JavaScript objects | 44 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| CoolType.SING.uniqueName | 34<br> | CVE-2010-2883<br> | CoolType.SING.uniqueName |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 39 |  
| /JavaScript | 39 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 1 |  
| /OpenAction | 1 |  
| /XFA | 41 |  
  
**JS vulneralibities:** {&#x27;name&#x27;: &#x27;getIcon&#x27;, &#x27;objects&#x27;: [44], &#x27;vuln_cve_list&#x27;: [&#x27;CVE-2009-0927&#x27;], &#x27;vuln_name&#x27;: &#x27;getIcon&#x27;}, {&#x27;name&#x27;: &#x27;Collab.collectEmailInfo&#x27;, &#x27;objects&#x27;: [44], &#x27;vuln_cve_list&#x27;: [&#x27;CVE-2007-5659&#x27;], &#x27;vuln_name&#x27;: &#x27;Collab.collectEmailInfo&#x27;}  
  
</details>  

---
  
<details>  
<summary><b>Detection 34/59</b>: for sample.pdf (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](json-output/peepdf/peepdf_67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac.json)*   
  
**SHA256:** `67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac`  
**SHA1:** adc2da1ee93f9708f92a86bfd03ea01c8d2befa9  
**MD5:** 45347f15c27099cd11b1f2097ef00578  
**PDF version:** 1.5  
**Filesize (in bytes):** 5672  
**Binary:** True  
**Number of objects:** 6  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6 |  
| Catalog | 1 |  
| Streams | 6 |  
| Encoded streams | 6 |  
| Error objects | 6 |  
| JavaScript objects | 6 |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 5 |  
| /JavaScript | 5 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /OpenAction | 1 |  
  
**JS vulneralibities:** {&#x27;name&#x27;: &#x27;util.printf&#x27;, &#x27;objects&#x27;: [6], &#x27;vuln_cve_list&#x27;: [&#x27;CVE-2008-2992&#x27;], &#x27;vuln_name&#x27;: &#x27;util.printf&#x27;}  
  
</details>  

---
  
<details>  
<summary><b>Detection 40/59</b>: for CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](json-output/peepdf/peepdf_870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38.json)*   
  
**SHA256:** `870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38`  
**SHA1:** 8b1c06c65698165429feb343ecbe653cc9d884f6  
**MD5:** 2e2124d77a3892566c7f2df207229960  
**PDF version:** 1.5  
**Filesize (in bytes):** 2685  
**Binary:** True  
**Number of objects:** 8  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8 |  
| Catalog | 7 |  
| Streams | 1 |  
| Encoded streams | 1 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| /EmbeddedFile | 1<br> |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 7 |  
| /XFA | 8 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 33/58</b>: for CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](json-output/peepdf/peepdf_94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc.json)*   
  
**SHA256:** `94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc`  
**SHA1:** 1ee780ab3b87fad953bebb4b0d068774e115e87d  
**MD5:** 3670a3f864bb4dcde9d399cc4e814798  
**PDF version:** 1.4  
**Filesize (in bytes):** 1083750  
**Binary:** True  
**Number of objects:** 60  
**Number of streams:** 20  
**Encrypted:** True  
  
### Footnotes  
[^1]: Report identification value is UUID as a 32-character hexadecimal string.
  
[^2]: See more information about ClamAV filetypes [in here.](https://www.clamav.net/documents/clamav-file-types#cltypes)
  
[^3]: The maliciousness of file is classified based on the objects it contains. Suspicious objects are thought to be: &#x27;/JS&#x27;, &#x27;/JavaScript&#x27;, &#x27;/AA&#x27;, &#x27;/OpenAction&#x27;, &#x27;/AcroForm&#x27;, &#x27;/JBIG2Decode&#x27;, &#x27;/RichMedia&#x27;, &#x27;/Launch&#x27;, &#x27;/EmbeddedFile&#x27;, &#x27;/XFA&#x27;, &#x27;/Colors &gt; 2^24&#x27; 
  
[^4]: VirusTotal was founded in 2004 as a free service that analyzes files and URLs for viruses, worms, trojans and other kinds of malicious content. Their goal is to make the internet a safer place through collaboration between members of the antivirus industry, researchers and end users of all kinds. [Virustotal](https://www.virustotal.com/)
  
