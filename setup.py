import setuptools
import os

long_description = None

if os.path.exists("README.md"):
    with open("README.md", "r") as fh:
        long_description = fh.read()

setuptools.setup(
    name="standardizer",
    version="0.0.1",
    author="CinCan",
    author_email="CinCan",
    description="Standardizer for Concource pipeline tool outputs",
    long_description=long_description if long_description else None,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/CinCan/tools-output-standardization",
    classifiers=[],
    packages=setuptools.find_packages(),
    entry_points={"console_scripts": ["standardizer=standardizer.__main__:main"]},
    install_requires=["tqdm>=4.28.1", "bs4>=0.0.1"],
)
