# Output standardization

## Standardizer installation (Ubuntu)

Install Python 3 and pip:
```
sudo apt install python3 python3-pip
```

Install standardizer as pip package globally:
```
git clone https://gitlab.com/CinCan/tools-output-standardization.git
cd tools-output-standardization
pip3 install .
```

Or just run without installation:
```
git clone https://gitlab.com/CinCan/tools-output-standardization.git
cd tools-output-standardization
python3 -m standardizer <arguments>
```

## Standardizer usage

Currently standardizer has two main features:
  * Generate Markdown report from result files of specific Concourse pipeline
  * Standardize single tool output data from plain text into to JSON format

```shell
usage: standardizer <command> [<args>]

Available commands are:

     markdown       To generate Markdown report of specific type from various tool result files.
     json           To standardize specific tool output into the JSON format.

Tool used to standardize output of various tools in JSON output format or as
Markdown report.

positional arguments:
  command     Subcommand to run. Each command has their own help.

optional arguments:
  -h, --help  show this help message and exit

```

### Markdown usage

This standardization project is aiming to provide framework for generating Markdown formatted files.

Currently tool can produce as an example Markdown report from [Document pipeline](https://gitlab.com/CinCan/pipelines/tree/master/document-pipeline).  

See [example.md](example.md)

However, report is purely plugin based, and Markdown output generation is supported for following tools:
  * ClamAV (ClamAV output JSON has two additional parameters: fileName, fileSHA256)
  * peepdf (JSON output)
  * pdfid (Result files must be in JSON, official PDFiD has no support for this yet)
  * strings (NOTE: results must be in JSON format, with following attributes: (fileSHA256, fileName, strings)
  * olevba (JSON output)
  * oledump (JSON output)
  * combination of jsunpack-n and peepdf's sctest results. Sctest's output in JSON

Currently single tools are not supported from command line. New pipeline report file need to be created, which uses wanted tool extensions. Documentation for that to be added later.


Example usage can be seen below:

` $ standardizer markdown `

Would provide following help:

```shell

No arguments provided for Markdown report generator.

usage: standardizer [-h] [-o [OUTPUT]] [-v] pipeline results_path

Tool used to generate Markdown report from various malware analysis tools
output result files. Main purpose is to generate report from pipelines used in
the CinCan project. (https://cincan.io)

positional arguments:
  pipeline              Give the name of pipeline whereof the report should be
                        generated. Currently available pipelines are:
                        document-pipeline
  results_path          Give the path of selected pipeline's results root
                        directory. Each directory for each tool's results is
                        found automatically by name. See 'conf.py' for more
                        details.

optional arguments:
  -h, --help            show this help message and exit
  -o [OUTPUT], --output [OUTPUT]
                        Output path/filename where into Markdown report will
                        be generated.
  -v, --verbose         Verbose, show all debug messages when constructing
                        report.


```

And by running:

`$ standardizer markdown document-pipeline /path/to/results/ -o my_report_filename`

Would produce the Markdown report if result files are found.

### JSON usage

Standardize single file containing binwalk output data:
```
standardizer json -i output/binwalk_output.txt -o output/binwalk_output_standardized.json -t binwalk
```

Standardize single file containing pdfxray_lite output data:
```
standardizer json -i output/pdfxray_lite_output.html -o output/pdfxray_lite_output_standardized.json -t pdfxray_lite
```

Standardize multiple files containing different output data:
```
standardizer json -i output/binwalk_output.txt,output/pdf2john_output.txt,output/pdfid_output.txt,output/pdfxray_lite_output.html,output/strings_output.txt -o output/binwalk_output_standardized.json,output/pdf2john_output_standardized.json,output/pdfid_output_standardized.json,output/pdfxray_lite_output_standardized.json,output/strings_output_standardized.json -t binwalk,pdf2john,pdfid,pdfxray_lite,strings
```

## Standardized json output

Preserved keys that must exist:

* `["raw"]` - Raw output
* `["parsed"]` - Custom parsed output depending on ran software
* `["metadata"]["unix_timestamp"]` - Current timestamp (Python example: `time.time()`)

Parsed output depending on ran software will be placed in `["parsed"]`.

## Tool output parsers

* [binwalk](docs/binwalk.md)
* [pdfid](docs/pdfid.md) #! This is deprecated, since it has build-in JSON support on forked version.
* [pdf2john](docs/pdf2john.md)
* [pdfxray_lite](docs/pdfxray_lite.md)
* [strings](docs/strings.md)