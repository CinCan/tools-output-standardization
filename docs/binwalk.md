# Binwalk example

Raw output:
```

Scan Time:     2019-02-11 12:17:38
Target File:   /samples/input/ELF_02BAA4707486A4DF3E1B096020DCC944
MD5 Checksum:  02baa4707486a4df3e1b096020dcc944
Signatures:    390

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
0             0x0             ELF, 32-bit LSB shared object, Intel 80386, version 1 (SYSV)
27971         0x6D43          Unix path: /var/run/utmp
28160         0x6E00          Base64 standard index table



```

Parsed json output:
```
{
    "raw": "\nScan Time:     2019-02-11 12:17:38\nTarget File:   /samples/input/ELF_02BAA4707486A4DF3E1B096020DCC944\nMD5 Checksum:  02baa4707486a4df3e1b096020dcc944\nSignatures:    390\n\nDECIMAL       HEXADECIMAL     DESCRIPTION\n--------------------------------------------------------------------------------\n0             0x0             ELF, 32-bit LSB shared object, Intel 80386, version 1 (SYSV)\n27971         0x6D43          Unix path: /var/run/utmp\n28160         0x6E00          Base64 standard index table\n\n\n",
    "parsed": {
        "table": {
            "decimal": [
                "0",
                "27971",
                "28160"
            ],
            "hexadecimal": [
                "0x0",
                "0x6D43",
                "0x6E00"
            ],
            "description": [
                "ELF, 32-bit LSB shared object, Intel 80386, version 1 (SYSV)",
                "Unix path: /var/run/utmp",
                "Base64 standard index table"
            ]
        },
        "scan_time": "2019-02-11 12:17:38",
        "target_file": "/samples/input/ELF_02BAA4707486A4DF3E1B096020DCC944",
        "md5_checksum": "02baa4707486a4df3e1b096020dcc944",
        "signatures": "390"
    },
    "metadata": {
        "unix_timestamp": 1550133429.7037227
    }
}
```