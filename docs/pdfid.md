# pdfid example

Raw output:
```
PDFiD 0.2.5 /samples/input/CVE-2013-0640_PDF_A7C89D433F737B3FDC45B9FFBC947C4D_A7C89D433F737B3FDC45B9FFBC947C4D
 PDF Header: %PDF-1.4
 obj                    8
 endobj                 8
 stream                 1
 endstream              2
 xref                   1
 trailer                1
 startxref              1
 /Page                  1
 /Encrypt               1
 /ObjStm                0
 /JS                    1
 /JavaScript            1
 /AA                    0
 /OpenAction            1
 /AcroForm              1
 /JBIG2Decode           0
 /RichMedia             0
 /Launch                0
 /EmbeddedFile          0
 /XFA                   1
 /URI                   0
 /Colors > 2^24         0

Triage plugin score:        1.00
Triage plugin instructions: Sample is likely malicious and requires further analysis
```

Parsed json output:
```
{
    "raw": "PDFiD 0.2.5 /samples/input/CVE-2013-0640_PDF_A7C89D433F737B3FDC45B9FFBC947C4D_A7C89D433F737B3FDC45B9FFBC947C4D\r\n PDF Header: %PDF-1.4\r\n obj                    8\r\n endobj                 8\r\n stream                 1\r\n endstream              2\r\n xref                   1\r\n trailer                1\r\n startxref              1\r\n /Page                  1\r\n /Encrypt               1\r\n /ObjStm                0\r\n /JS                    1\r\n /JavaScript            1\r\n /AA                    0\r\n /OpenAction            1\r\n /AcroForm              1\r\n /JBIG2Decode           0\r\n /RichMedia             0\r\n /Launch                0\r\n /EmbeddedFile          0\r\n /XFA                   1\r\n /URI                   0\r\n /Colors > 2^24         0\r\n\r\nTriage plugin score:        1.00\r\nTriage plugin instructions: Sample is likely malicious and requires further analysis",
    "parsed": {
        "pdfid_version": "0.2.5",
        "file_path": "/samples/input/CVE-2013-0640_PDF_A7C89D433F737B3FDC45B9FFBC947C4D_A7C89D433F737B3FDC45B9FFBC947C4D",
        "pdf_header": "%PDF-1.4",
        "obj": 8,
        "endobj": 8,
        "stream": 1,
        "endstream": 2,
        "xref": 1,
        "trailer": 1,
        "startxref": 1,
        "/Page": 1,
        "/Encrypt": 1,
        "/ObjStm": 0,
        "/JS": 1,
        "/JavaScript": 1,
        "/AA": 0,
        "/OpenAction": 1,
        "/AcroForm": 1,
        "/JBIG2Decode": 0,
        "/RichMedia": 0,
        "/Launch": 0,
        "/EmbeddedFile": 0,
        "/XFA": 1,
        "/URI": 0,
        "/Colors > 2^24": 0,
        "triage_plugin_score": 1.0,
        "triage_plugin_instructions": "Sample is likely malicious and requires further analysis"
    },
    "metadata": {
        "unix_timestamp": 1550133738.55416
    }
}
```