# pdf2john example

Raw output:
```
/samples/encrypted_sample.pdf:$pdf$1*2*40*-8*1*16*5eacd10e5377cfdcff6309326ade8046*32*dcb914245f416b46f39adbfa603029306c473e38b59191a271e91743a4ab0606*32*8bced2327ab0c55e4fa0bf7a6e29f7bb77e48e4579ab022fa6968256bef2677f

```

Parsed json output:
```
{
    "raw": "/samples/encrypted_sample.pdf:$pdf$1*2*40*-8*1*16*5eacd10e5377cfdcff6309326ade8046*32*dcb914245f416b46f39adbfa603029306c473e38b59191a271e91743a4ab0606*32*8bced2327ab0c55e4fa0bf7a6e29f7bb77e48e4579ab022fa6968256bef2677f\n",
    "parsed": {
        "file_path": "/samples/encrypted_sample.pdf",
        "pdf_format": "$pdf$1*2*40*-8*1*16",
        "pdf_hash": "$pdf$1*2*40*-8*1*16*5eacd10e5377cfdcff6309326ade8046*32*dcb914245f416b46f39adbfa603029306c473e38b59191a271e91743a4ab0606*32*8bced2327ab0c55e4fa0bf7a6e29f7bb77e48e4579ab022fa6968256bef2677f"
    },
    "metadata": {
        "unix_timestamp": 1550133429.708326
    }
}
```
