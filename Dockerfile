FROM python:3-alpine AS compile-image

RUN mkdir -p /usr/src/standardizer
WORKDIR /usr/src/standardizer

RUN apk update && apk upgrade
RUN apk add gcc libffi-dev musl-dev openssl-dev git
RUN pip3 install --upgrade pip
COPY . /usr/src/standardizer/

RUN pip3 install --user --no-cache-dir .

FROM python:3-alpine AS build-image

COPY --from=compile-image /root/.local /root/.local

RUN apk add --no-cache git

# Make sure .local is usable
ENV PATH=/root/.local/bin:$PATH

ENTRYPOINT ["standardizer"]

